import { useContext } from 'react';
import AuthenticationContext from '~/store/authentication-context';
import useAuthRefresh from '~/hooks/useAuthRefresh';

interface Session {
  accessToken: string | null | undefined;
  status: 'authorized' | 'unauthorized';
}

export default function useAuthSession(): Session {
  useAuthRefresh();

  const { accessToken } = useContext(AuthenticationContext);

  return {
    accessToken,
    status: accessToken != null ? 'authorized' : 'unauthorized',
  };
}
