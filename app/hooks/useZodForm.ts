import { useFormik } from 'formik';
import * as z from 'zod';

/**
 * Utilizes formik validation logic for easy error handling. Submission is still handled through standard
 * Remix Form components.
 *
 * @param initialValues - initial form values
 * @param zodSchema - validation schema from zod
 */
export default function useZodForm(
  initialValues: Record<string, any>,
  zodSchema: z.Schema
) {
  return useFormik({
    initialValues,
    validate: (values) => {
      const result = zodSchema.safeParse(values);
      if (result.success) {
        return {};
      }
      return result.error.formErrors.fieldErrors;
    },
    onSubmit: () => {},
  });
}
