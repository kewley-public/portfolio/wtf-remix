import { DateTime } from 'luxon';

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL || 'http://127.0.0.1:3000';

export default function useEventsApi(isDemoMode: boolean) {
  async function fetchEvents(
    start: DateTime,
    end: DateTime
  ): Promise<App.Paginated<App.Event> | null> {
    try {
      const url = new URL(`${BASE_URL}/api/events`);
      url.searchParams.set('start', start.toString());
      url.searchParams.set('end', end.toString());
      const fetchResponse = await fetch(url.toString(), {
        headers: {
          // @ts-expect-error This property is created during next auth login
          Authorization: `Bearer ${session!!.accessToken}`,
        },
      });
      const jsonResponse = await fetchResponse.json();
      if (!fetchResponse.ok) {
        console.error('Unable to fetch items', jsonResponse);
        return null;
      }
      return jsonResponse.data;
    } catch (e: any) {
      console.error(e);
    }
    return null;
  }

  return {
    fetchEvents,
  };
}
