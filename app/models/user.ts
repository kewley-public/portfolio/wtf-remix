import { z } from 'zod';

export const UserSchema = z.object({
  id: z.string(),
  email: z.string().email(),
  verified: z.boolean(),
  userName: z.string().optional().nullable(),
  passwordHash: z.string(),
  googleOAuthId: z.string().optional().nullable(),
  created: z.date(),
});

export const UserAuthFormCredentialsSchema = z.object({
  email: z.string().email(),
  password: z.string(),
});

export const UserAuthFormGoogleSchema = z.object({
  email: z.string().email(),
  googleOAuthId: z.string(),
});

export const UserAuthFormSchema = z.object({
  email: z.string().email(),
  password: z.string(),
  userName: z.string().optional().nullable(),
  googleOAuthId: z.string().optional().nullable(),
});
