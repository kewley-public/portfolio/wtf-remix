import Table from '~/components/ui/Table';
import { useEffect, useState } from 'react';
import { useNavigate } from '@remix-run/react';

interface Props {
  funds: App.Fund[];
  categories: App.Category[];
}

export default function FundsTable({ funds, categories }: Props) {
  const navigate = useNavigate();
  const [categoryMap, setCategoryMap] = useState(new Map<string, string>());

  useEffect(() => {
    const newCategoryMap = new Map();
    categories.forEach((c) => {
      newCategoryMap.set(c.id, c.description);
    });
    setCategoryMap(newCategoryMap);
  }, [categories]);

  const formatters = {
    categoryId: (value: string) => {
      return categoryMap.get(value) || 'Unknown';
    },
  };

  const formattedHeaders = {
    categoryId: () => 'Category',
  };

  function onSelectHandler(item: App.Fund) {
    const fundItem = funds.find((e) => e.id === item.id);
    if (fundItem) {
      navigate(`/settings/funds/${fundItem.id}`);
    }
  }

  return (
    <Table
      filteredHeaders={['id']}
      items={funds}
      onSelect={onSelectHandler}
      formatters={formatters}
      formattedHeaders={formattedHeaders}
      title='Funds'
    />
  );
}
