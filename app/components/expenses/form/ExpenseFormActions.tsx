import AppButton from '~/components/ui/AppButton';
import { useNavigation } from '@remix-run/react';
import LoadingSpinner from '~/components/ui/LoadingSpinner';

interface Props {
  valid: boolean;
  dirty: boolean;
  editing: boolean;
  onDeleteClick: () => void;
  onCancelClick: () => void;
}

export default function ExpenseFormActions({
  valid,
  dirty,
  editing,
  onDeleteClick,
  onCancelClick,
}: Props) {
  const navigation = useNavigation();

  const isSubmitting = navigation.state !== 'idle';
  const saveButtonText = editing ? 'Save' : 'Create';
  if (isSubmitting) {
    return (
      <div className='flex items-center justify-center mt-5'>
        <LoadingSpinner size='lg'>Saving...</LoadingSpinner>
      </div>
    );
  }
  return (
    <>
      <div className='flex justify-between items-center px-3 py-2'>
        <AppButton
          type='submit'
          color='green'
          full
          className='mr-2'
          disabled={!valid || !dirty}
        >
          {saveButtonText}
        </AppButton>

        <AppButton
          onClick={onCancelClick}
          color='white'
          outline
          full
          type='button'
          className='ml-2'
        >
          Cancel
        </AppButton>
      </div>

      {editing && (
        <div className='flex items-center mt-5'>
          <AppButton
            onClick={onDeleteClick}
            color='red'
            className='ml-2'
            full
            type='button'
          >
            Delete
          </AppButton>
        </div>
      )}
    </>
  );
}
