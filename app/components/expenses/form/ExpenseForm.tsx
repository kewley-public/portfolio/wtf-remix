import AppSelection from '~/components/ui/form/AppSelection';
import AppDatePicker from '~/components/ui/form/AppDatePicker';
import AppInput from '~/components/ui/form/AppInput';
import useZodForm from '~/hooks/useZodForm';
import { DateTime } from 'luxon';
import { CreateExpenseSchema, UpdateExpenseSchema } from '~/models/expense';
import { useState } from 'react';
import { Form } from '@remix-run/react';
import ExpenseDeleteForm from '~/components/expenses/form/ExpenseDeleteForm';
import ExpenseFormActions from '~/components/expenses/form/ExpenseFormActions';

interface Props {
  categories: App.Category[];
  existingExpense?: App.Expense;
  onCancel: () => void;
}

export default function ExpenseForm({
  categories,
  existingExpense,
  onCancel,
}: Props) {
  const [showDeleteForm, setShowDeleteForm] = useState<boolean>(false);

  const initialValues = existingExpense
    ? {
        ...existingExpense,
        date: DateTime.fromJSDate(new Date(existingExpense.date)).toJSDate(),
      }
    : {
        description: '',
        amount: 0,
        categoryId: categories.length ? categories[0].id : null,
        date: DateTime.now().toJSDate(),
      };
  const titleText = existingExpense
    ? `Edit ${existingExpense.description}`
    : 'Add Expense';

  const formik = useZodForm(
    initialValues,
    existingExpense ? UpdateExpenseSchema : CreateExpenseSchema
  );

  function toggleDelete() {
    setShowDeleteForm(!showDeleteForm);
  }

  if (showDeleteForm) {
    return <ExpenseDeleteForm onCancel={toggleDelete} />;
  }

  return (
    <Form
      id='event-form'
      method={existingExpense ? 'put' : 'post'}
      className='w-full p-4'
    >
      <div className='text-primary-50 font-bold text-2xl text-center'>
        {titleText}
      </div>
      <AppSelection
        label='Category'
        id='categoryId'
        name='categoryId'
        items={categories.map((c) => ({
          id: c.id!!,
          display: c.description,
        }))}
        required
        value={formik.values.categoryId}
        onChange={formik.handleChange}
        error={formik.errors.categoryId}
      />

      <AppDatePicker
        label='Date'
        id='date'
        name='date'
        required
        selected={formik.values.date}
        onChange={(date) => formik.setFieldValue('date', date)}
        error={formik.errors.date}
      />

      <AppInput
        label='Description'
        id='description'
        name='description'
        required
        value={formik.values.description}
        onChange={formik.handleChange}
        error={formik.errors.description}
      />

      <AppInput
        label='Amount'
        id='amount'
        name='amount'
        type='number'
        required
        value={formik.values.amount}
        onChange={formik.handleChange}
        error={formik.errors.amount}
      />

      <ExpenseFormActions
        valid={formik.isValid}
        dirty={formik.dirty}
        editing={existingExpense != null}
        onDeleteClick={toggleDelete}
        onCancelClick={onCancel}
      />
    </Form>
  );
}
