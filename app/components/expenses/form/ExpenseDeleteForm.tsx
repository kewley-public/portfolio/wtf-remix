import { Form, useNavigation } from '@remix-run/react';
import AppButton from '~/components/ui/AppButton';
import LoadingSpinner from '~/components/ui/LoadingSpinner';

interface Props {
  onCancel: () => void;
}

export default function ExpenseDeleteForm({ onCancel }: Props) {
  return (
    <main className='w-full p-4'>
      <header>
        <h1 className='text-primary-50 font-bold text-2xl text-center'>
          Are You Sure?
        </h1>
        <h2 className='text-lg text-primary-100 text-center'>
          There is no going back
        </h2>
      </header>

      <section>
        <Form
          method='delete'
          id='expense-delete-form'
          className='flex justify-between items-center px-3 py-2 mt-5'
        >
          <DeleteFormActions onCancel={onCancel} />
        </Form>
      </section>
    </main>
  );
}

interface Props {
  onCancel: () => void;
}
function DeleteFormActions({ onCancel }: Props) {
  const navigation = useNavigation();

  const isSubmitting = navigation.state !== 'idle';
  if (isSubmitting) {
    return (
      <div className='w-full'>
        <LoadingSpinner size='lg'>Deleting...</LoadingSpinner>
      </div>
    );
  }

  return (
    <>
      <AppButton
        type='submit'
        color='red'
        full
        className='mr-2'
        disabled={isSubmitting}
      >
        Yes
      </AppButton>

      <AppButton
        onClick={onCancel}
        color='white'
        outline
        full
        type='button'
        className='ml-2'
        disabled={isSubmitting}
      >
        No
      </AppButton>
    </>
  );
}
