import { useEffect, useState } from 'react';
import { calculateTotalPerMonth, groupExpensesByMonth } from '~/lib/util';
import { Bar } from 'react-chartjs-2';
import LoadingSpinner from '~/components/ui/LoadingSpinner';

interface Props {
  expenses: App.Expense[];
}

export default function ExpensesByMonth({ expenses }: Props) {
  const [data, setData] = useState(null);
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        position: 'top',
        labels: {
          color: 'white', // Correct way to set legend text color
        },
      },
    },
    scales: {
      x: {
        ticks: {
          color: 'white', // Sets the x-axis labels color
        },
        title: {
          display: true,
          text: 'Month',
          color: 'white', // Sets the x-axis title color
        },
      },
      y: {
        ticks: {
          color: 'white', // Sets the y-axis labels color
        },
        title: {
          display: true,
          text: 'Expenses',
          color: 'white', // Sets the y-axis title color
        },
      },
    },
  };

  useEffect(() => {
    const expensesByMonth = groupExpensesByMonth(expenses);
    const labels = Object.keys(expensesByMonth);

    labels.reverse();

    const expenseTotalsPerMonth = calculateTotalPerMonth(expensesByMonth);

    setData({
      labels,
      datasets: [
        {
          label: 'Expense Breakdown By Month',
          data: labels.map((label) => expenseTotalsPerMonth[label]),
          backgroundColor: labels.map(() => 'rgb(77,109,233)'),
          borderColor: labels.map(() => 'rgb(13,29,100)'),
        },
      ],
      borderWidth: 1,
    });
  }, [expenses]);

  if (data) {
    return (
      <section className='h-[40rem]'>
        <Bar options={options} data={data} />
      </section>
    );
  }
  return (
    <LoadingSpinner size='lg'>
      Loading Expense Breakdown By Month...
    </LoadingSpinner>
  );
}
