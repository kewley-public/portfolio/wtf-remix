import { Line } from 'react-chartjs-2';
import { useEffect, useState } from 'react';
import { groupExpensesByMonth } from '~/lib/util';
import LoadingSpinner from '~/components/ui/LoadingSpinner';
import colors from '../../../lib/colors';

interface Props {
  expenses: App.Expense[];
  categories: App.Category[];
}
export default function ExpensesByCategory({ expenses, categories }: Props) {
  const [data, setData] = useState(null);
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        position: 'top',
        labels: {
          color: 'white', // Correct way to set legend text color
        },
      },
      title: {
        display: true,
        text: 'Expenses By Category Per Month',
        color: 'white', // Correct way to set title text color
      },
    },
    scales: {
      x: {
        ticks: {
          color: 'white', // Sets the x-axis labels color
        },
        title: {
          display: true,
          text: 'Month',
          color: 'white', // Sets the x-axis title color
        },
      },
      y: {
        ticks: {
          color: 'white', // Sets the y-axis labels color
        },
        title: {
          display: true,
          text: 'Expenses',
          color: 'white', // Sets the y-axis title color
        },
      },
    },
  };

  useEffect(() => {
    const expensesByMonth = groupExpensesByMonth(expenses);
    const labels = Object.keys(expensesByMonth);

    labels.reverse();

    const dataSets = categories.map((category: App.Category) => ({
      label: category.description,
      data: labels.map((label) =>
        expensesByMonth[label]
          .filter((e) => e.categoryId === category.id)
          .reduce((total, expense) => total + expense.amount, 0)
      ),
      borderColor: colors.category[category.colorCodeCategory.toLowerCase()],
      backgroundColor:
        colors.category[category.colorCodeCategory.toLowerCase()],
    }));

    setData({
      labels,
      datasets: dataSets,
    });
  }, [categories, expenses]);

  if (data) {
    return (
      <section className='h-[40rem]'>
        <Line options={options} data={data} />
      </section>
    );
  }

  return (
    <LoadingSpinner size='lg'>
      Loading Expenses By Category Per Month...
    </LoadingSpinner>
  );
}
