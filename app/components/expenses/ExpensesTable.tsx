import Table from '~/components/ui/Table';
import { formatCurrency } from '~/lib/util';
import { useEffect, useState } from 'react';
import { useNavigate } from '@remix-run/react';
import AppButton from '~/components/ui/AppButton';
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa';

interface Props {
  expenses: App.Expense[];
  categories: App.Category[];
}

export default function ExpensesTable({ expenses, categories }: Props) {
  const navigate = useNavigate();
  const [categoryMap, setCategoryMap] = useState<Map<string, string>>(
    new Map()
  );

  function onSelectHandler(item: App.Expense) {
    const expenseItem = expenses.find((e) => e.id === item.id);
    if (expenseItem) {
      navigate(`/wallet/expenses/${expenseItem.id}`);
    }
  }

  useEffect(() => {
    const newCategoryMap = new Map();
    categories.forEach((c) => {
      newCategoryMap.set(c.id, c.description);
    });
    setCategoryMap(newCategoryMap);
  }, [categories]);

  const formatters = {
    date: (value: Date) => {
      return new Date(value).toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
      });
    },
    amount: formatCurrency,
    categoryId: (value: string) => {
      return categoryMap.get(value) || 'Unknown';
    },
  };

  const formattedHeaders = {
    categoryId: () => 'Category',
  };

  return (
    <div className='flex flex-col'>
      <div className='flex items-center justify-between mt-5 mx-7'>
        <AppButton color='white' outline>
          <FaArrowLeft />
        </AppButton>

        <AppButton color='white' outline>
          <FaArrowRight />
        </AppButton>
      </div>
      <Table
        filteredHeaders={['id', 'userId', 'fundId']}
        items={expenses}
        onSelect={onSelectHandler}
        formatters={formatters}
        formattedHeaders={formattedHeaders}
        title='Expenses'
      />
      <div className='flex items-center justify-center mt-5 mx-7'>
        <AppButton
          color='white'
          outline
          full
          onClick={() => navigate('/wallet/expenses/add')}
        >
          Add
        </AppButton>
      </div>
    </div>
  );
}
