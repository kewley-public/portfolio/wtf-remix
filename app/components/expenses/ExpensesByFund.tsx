import LoadingSpinner from '~/components/ui/LoadingSpinner';
import { useEffect, useState } from 'react';
import { DateTime } from 'luxon';
import colors from '../../../lib/colors';
import { Doughnut } from 'react-chartjs-2';

interface Props {
  funds: App.Fund[];
  categories: App.Category[];
  expenses: App.Expense[];
}

export default function ExpensesByFund({ funds, expenses, categories }: Props) {
  const [totalFunds, setTotalFunds] = useState(0);
  const [totalSpent, setTotalSpent] = useState(0);
  const [data, setData] = useState(null);

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        position: 'top',
        labels: {
          color: 'white', // Correct way to set legend text color
        },
      },
      title: {
        display: true,
        text: 'Expenses per Fund for month',
        color: 'white', // Correct way to set title text color
      },
    },
  };

  useEffect(() => {
    const thisMonth = DateTime.now().startOf('month');
    const expensesThisMonth = expenses.filter((expense) => {
      const diffInMonths = DateTime.fromJSDate(new Date(expense.date)).diff(
        thisMonth,
        'month'
      ).months;
      return diffInMonths > 0 && diffInMonths < 1;
    });

    const markerColors = [];
    const data: number[] = [];
    const labels: string[] = [];

    let totalBudget = 0;
    funds.forEach((fund: App.Fund) => {
      const totalAmountPerFund = expensesThisMonth
        .filter((e: App.Expense) => e.fundId === fund.id)
        .map((e: App.Expense) => e.amount)
        .reduce((sum, next) => sum + next, 0);

      const category = categories.find((c) => c.id === fund.categoryId);

      data.push(Math.min(totalAmountPerFund, fund.amount));
      if (totalAmountPerFund > fund.amount) {
        labels.push(`${fund.description}**`);
      } else {
        labels.push(fund.description);
      }

      markerColors.push(colors.category[category?.colorCodeCategory]);
      totalBudget += fund.amount;
    });

    setTotalFunds(totalBudget);

    const _totalSpentOnFunds = data.reduce((sum, next) => sum + next, 0);
    data.push(Math.max(totalBudget - _totalSpentOnFunds, 0));
    labels.push('Remaining');
    markerColors.push('rgb(77,112,243)');

    const _totalSpent = expenses.reduce(
      (sum, expense) => sum + expense.amount,
      0
    );
    setTotalSpent(_totalSpent);

    setData({
      labels,
      datasets: [
        {
          label: 'Test',
          data,
          backGroundColor: markerColors,
          hoverOffset: 4,
        },
      ],
    });
  }, []);

  if (data) {
    return (
      <main className='w-full'>
        <header className='flex flex-col justify-start items-start'>
          <h1 className='font-bold text-lg'>
            Total Funds: <span className='text-green-500'>$ {totalFunds}</span>
          </h1>
          <h1 className='font-bold text-lg'>
            Total Spent:{' '}
            <span className='text-red-400'>${totalSpent.toFixed(2)}</span>
          </h1>
        </header>
        <section className='h-[40rem]'>
          <Doughnut data={data} options={options} />
        </section>
      </main>
    );
  }

  return (
    <LoadingSpinner size='lg'>
      Loading Expenses By Fund This Month...
    </LoadingSpinner>
  );
}
