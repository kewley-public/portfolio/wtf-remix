import AppNavLink from '~/components/navigation/AppNavLink';

export default function WalletHeader() {
  return (
    <header className='flex items-center justify-center space-x-4'>
      <AppNavLink to='/wallet/expenses'>Expenses</AppNavLink>
      <AppNavLink to='/wallet/analysis'>Analysis</AppNavLink>
      <AppNavLink to='/wallet/events'>Events</AppNavLink>
    </header>
  );
}
