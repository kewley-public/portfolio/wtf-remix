import AppNavLink from '~/components/navigation/AppNavLink';
import useAuthSession from '~/hooks/useAuthSession';
import { Form } from '@remix-run/react';
import { FaSignInAlt, FaSignOutAlt } from 'react-icons/fa';

export default function NavLinks() {
  const { status } = useAuthSession();

  if (status === 'authorized') {
    return (
      <>
        <AppNavLink to='/wallet/expenses' hasNestedRoutes>
          Wallet
        </AppNavLink>

        <AppNavLink to='/settings'>Settings</AppNavLink>

        <Form method='delete' id='logout-form' action='/logout'>
          <button className='font-bold text-primary-50 hover:text-primary-100 p-2 text-xl hover:border-b-2 hover:border-b-primary-200'>
            <FaSignOutAlt />
          </button>
        </Form>
      </>
    );
  } else {
    return (
      <>
        <AppNavLink to='/demo'>Demo</AppNavLink>

        <AppNavLink to='/auth?mode=login' icon>
          <FaSignInAlt />
        </AppNavLink>
      </>
    );
  }
}
