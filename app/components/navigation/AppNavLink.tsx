import { PropsWithChildren } from 'react';
import { NavLink, useLocation } from '@remix-run/react';

interface Props extends PropsWithChildren {
  to: string;
  hasNestedRoutes?: boolean;
  icon?: boolean;
}

export default function AppNavLink({
  to,
  children,
  hasNestedRoutes = false,
  icon = false,
}: Props) {
  const location = useLocation();

  // Define the classes
  const baseClass = 'decoration-0 text-white font-bold px-4 py-2';
  const hoverClass =
    'hover:bg-clip-text hover:text-primary-200 hover:border-b-2 hover:border-b-primary-200';
  const activeClass =
    'bg-clip-text text-primary-200 border-b-2 border-primary-200';

  const iconClass = icon ? 'text-xl' : '';

  const base = `${baseClass} ${hoverClass} ${iconClass}`;

  // Function to determine if the link should have the active class
  const getClassName = ({ isActive }: { isActive: boolean }) => {
    // Check if the current path starts with the link's to path or if the link is active
    const isBaseRouteActive =
      hasNestedRoutes &&
      location.pathname.split('/')[1].startsWith(to.split('/')[1]);
    return isActive || isBaseRouteActive ? `${base} ${activeClass}` : base;
  };

  return (
    <NavLink to={to} className={getClassName}>
      {children}
    </NavLink>
  );
}
