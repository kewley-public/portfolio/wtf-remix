import { Link } from '@remix-run/react';
import NavLinks from '~/components/navigation/NavLinks';

export default function MainHeader() {
  return (
    <header className='flex flex-row items-center justify-between flex-wrap bg-transparent fixed top-0 w-full z-10 p-2 backdrop-filter backdrop-blur-md'>
      <Link to='/' className='flex-shrink-0 flex items-center'>
        <img src='/images/logo.png' alt='Wallet Logo' width={60} height={25} />
        <h1 className='text-2xl font-bold'>Where&apos;s the Funds?!</h1>
      </Link>

      <section className='flex items-center justify-end space-x-4'>
        <NavLinks />
      </section>
    </header>
  );
}
