import { FaExclamationCircle } from 'react-icons/fa';
import { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren {
  title: string;
}

function Error({ title, children }: Props) {
  return (
    <main className='h-screen flex flex-col text-xl items-center justify-center'>
      <header className='flex items-center justify-center'>
        <FaExclamationCircle className='text-2xl text-red-500' />
      </header>
      <h2 className='mx-1 text-2xl font-bold text-primary-50'>{title}</h2>
      {children}
    </main>
  );
}

export default Error;
