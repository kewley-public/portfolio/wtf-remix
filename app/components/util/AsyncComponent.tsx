import * as React from 'react';
import { Suspense } from 'react';
import { Await, useNavigate } from '@remix-run/react';
import LoadingSpinner from '~/components/ui/LoadingSpinner';
import { FaExclamationCircle } from 'react-icons/fa';
import AppButton from '~/components/ui/AppButton';

interface Props {
  fallback?: React.ReactNode;
  fallbackText?: string;
  resolve: Promise<any>;
  children(promiseResult: any): React.ReactNode;
}

/**
 * Useful component for showing loading state while the LoaderFunction is loading data utilizing the
 * defer Response utility offered by react: https://beta.reactrouter.com/en/dev/guides/deferred
 *
 * Usage:
 * ```ts
 * import { LoaderFunction, LoaderFunctionArgs } from '@remix-run/node';
 * import { useLoaderData, defer } from '@remix-run/react';
 * import AsyncComponent from '~/components/util/AsyncComponent';
 *
 * export default function SomeComponent() {
 *    const {resultOne, resultTwo} = useLoaderData<typeof loader>();
 *
 *    return (
 *      <AsyncComponent fallbackText="Loading data..." resolve={Promise.all([resultOne, resultTwo])}>
 *        {([resultOne, resultTwo]) =>
 *          (<Component resultOne={resultOne} resultTwo={resultTwo} />)
 *        }
 *      </AsyncComponent>
 *    );
 * }
 *
 * export const loader: LoaderFunction = async ({ request }: LoaderFunctionArgs) => {
 *    const data = Promise.all([getDataOne(), getDataTwo()]);
 *    return defer({resultOne: getDataOne(), resultTwo: getDataTwo()}, {headers: { 'Cache-Control': 'max-age=3' }});
 * }
 * ```
 * @param fallback - fallback component that is utilized, default is LoadingSpinner
 * @param fallbackText - loading text that is utilized, default is 'Loading...'
 * @param resolve - the promise to resolve that we are suspending/awaiting on
 * @param children - the component to load once we are done
 * @constructor
 */
export default function AsyncComponent({
  fallback,
  fallbackText,
  resolve,
  children,
}: Props) {
  return (
    <Suspense
      fallback={
        fallback || (
          <LoadingSpinner size='lg'>
            {fallbackText || 'Loading...'}
          </LoadingSpinner>
        )
      }
    >
      <Await resolve={resolve} errorElement={<ErrorElement />}>
        {(result) => children(result)}
      </Await>
    </Suspense>
  );
}

function ErrorElement() {
  const navigate = useNavigate();
  return (
    <main className='flex flex-col text-xl items-center justify-center'>
      <header className='flex items-center justify-center'>
        <FaExclamationCircle className='text-2xl text-red-500' />
      </header>
      <h2 className='mx-1 text-2xl font-bold text-primary-50'>Uh Oh!</h2>
      <h2 className='mx-1 text-2xl font-bold text-primary-50'>
        Unknown error occurred please try reloading
      </h2>
      <AppButton onClick={() => navigate(0)}>Reload</AppButton>
    </main>
  );
}
