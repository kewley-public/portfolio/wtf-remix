import { Form } from '@remix-run/react';
import { FaFacebook, FaGoogle, FaTwitch } from 'react-icons/fa';

interface Props {
  type: 'google' | 'twitch' | 'facebook';
}

export default function ThirdPartySignIn({ type }: Props) {
  let icon = null;
  switch (type) {
    case 'facebook':
      icon = <FaFacebook />;
      break;
    case 'google':
      icon = <FaGoogle />;
      break;
    case 'twitch':
      icon = <FaTwitch />;
      break;
  }
  return (
    <Form method='post' action={`/auth/${type}`} id={`${type}-sign-in`}>
      <button className='text-primary-50 text-4xl hover:text-primary-200 hover:cursor-pointer'>
        {icon}
      </button>
    </Form>
  );
}
