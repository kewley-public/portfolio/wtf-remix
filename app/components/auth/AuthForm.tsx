import { FaLock, FaUserPlus } from 'react-icons/fa';
import {
  Form,
  Link,
  useActionData,
  useNavigation,
  useSearchParams,
} from '@remix-run/react';
import { ZodIssue } from 'zod';
import AppInput from '~/components/ui/form/AppInput';
import AppButton from '~/components/ui/AppButton';
import ThirdPartySignIn from '~/components/auth/ThirdPartySignIn';

interface Errors {
  email: string | null | undefined;
  password: string | null | undefined;
  serverError: string | null | undefined;
  confirmEmail?: string | null | undefined;
  confirmPassword?: string | null | undefined;
}

function AuthForm() {
  const [searchParams] = useSearchParams();
  const navigation = useNavigation();
  const validationErrors = useActionData<{ errors: ZodIssue[] } | undefined>();
  const authMode = searchParams.get('mode') || 'login';
  const failed = searchParams.get('failed');

  const creatingUser = authMode === 'register';

  const submitButtonCaption = creatingUser ? 'Create User' : 'Login';
  const toggleButtonCaption = creatingUser
    ? 'Log in with existing user'
    : 'Create a new user';

  const isSubmitting = navigation.state !== 'idle';
  const buttonText = isSubmitting ? 'Authenticating...' : submitButtonCaption;

  const errors: Errors = {
    email: null,
    confirmEmail: null,
    password: null,
    confirmPassword: null,
    serverError: null,
  };
  if (validationErrors?.errors) {
    validationErrors.errors.forEach((e: ZodIssue) => {
      const message = e.message;
      if (e.path.includes('email')) {
        errors.email = message;
      } else if (e.path.includes('confirmEmail')) {
        errors.confirmEmail = message;
      } else if (e.path.includes('password')) {
        errors.password = message;
      } else if (e.path.includes('confirmPassword')) {
        errors.confirmPassword = message;
      } else if (e.path.includes('serverError')) {
        errors.serverError = message;
      }
    });
  }

  return (
    <main className='mx-auto mt-5 max-w-md rounded-xl shadow-lg bg-primary-700 p-6 flex flex-col items-center'>
      <section>
        <Form
          method='post'
          className='flex flex-col items-center'
          action={`/auth/${authMode}`}
          id='auth-form'
        >
          <div className='text-2xl my-2 text-primary-50 shadow-2xl'>
            {creatingUser ? <FaUserPlus /> : <FaLock />}
          </div>
          <h2 className='text-xl lg:text-2xl font-bold text-primary-50 shadow-md'>
            {submitButtonCaption}
          </h2>
          <p className='mt-1 text-md text-primary-100'>
            Welcome back! Please enter your details to sign in.
          </p>

          <AppInput
            label='Your Email'
            id='email'
            name='email'
            type='email'
            error={errors.email}
          />

          {creatingUser && (
            <AppInput
              label='Confirm Email'
              id='confirmEmail'
              name='confirmEmail'
              type='email'
              error={errors.confirmEmail}
            />
          )}

          <AppInput
            label='Password'
            id='password'
            name='password'
            type='password'
            error={errors.password}
          />

          {creatingUser && (
            <AppInput
              label='Confirm Password'
              id='confirmPassword'
              type='password'
              name='password'
              error={errors.confirmPassword}
            />
          )}

          <div className='flex flex-col w-full'>
            <AppButton full>{buttonText}</AppButton>
          </div>
        </Form>
      </section>

      {!creatingUser && (
        <section className='flex flex-row mt-5 space-x-5 p-2 justify-center items-center'>
          <ThirdPartySignIn type='google' />

          <ThirdPartySignIn type='facebook' />

          <ThirdPartySignIn type='twitch' />
        </section>
      )}

      <section className='mt-5 flex flex-col'>
        <Link
          className='text-primary-50 text-center hover:text-primary-200'
          to={creatingUser ? `?mode=login` : '?mode=register'}
        >
          {toggleButtonCaption}
        </Link>
        {failed && (
          <span className='text-sm text-red-200 text-center'>
            Unable to login, check your credentials
          </span>
        )}
      </section>
    </main>
  );
}

export default AuthForm;
