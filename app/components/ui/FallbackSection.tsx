import LoadingSpinner from '~/components/ui/LoadingSpinner';

export default function FallbackSection({
  loadingText,
}: {
  loadingText: string;
}) {
  return (
    <LoadingSpinner
      size='lg'
      className='flex flex-row items-center justify-center gap-2'
    >
      {loadingText}
    </LoadingSpinner>
  );
}
