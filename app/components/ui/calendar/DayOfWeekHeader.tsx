import { PropsWithChildren } from 'react';

export default function DayOfWeekHeader({ children }: PropsWithChildren) {
  return <h1 className='text-center font-bold text-primary-700'>{children}</h1>;
}
