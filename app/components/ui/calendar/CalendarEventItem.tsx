interface Props {
  event: App.Event;
  colorCategory: string;
  onClick: () => void;
}

export default function CalendarEventItem({
  event,
  colorCategory,
  onClick,
}: Props) {
  const backgroundColor = {
    ONE: 'bg-category-one',
    TWO: 'bg-category-two',
    THREE: 'bg-category-three',
    FOUR: 'bg-category-four',
    FIVE: 'bg-category-five',
    SIX: 'bg-category-six',
  }[colorCategory];
  return (
    <li
      className={`has-tooltip w-full p-1 text-center opacity-90 text-xs font-bold rounded-lg my-1 ${backgroundColor}`}
    >
      <button className='w-full h-full' onClick={onClick}>
        <span
          className={`tooltip rounded shadow-lg p-1 -mt-8 ${backgroundColor}`}
        >
          $ {event.amount}
        </span>
        {event.description}
      </button>
    </li>
  );
}
