import AppButton from '~/components/ui/AppButton';
import DayOfWeekHeader from '~/components/ui/calendar/DayOfWeekHeader';
import BlankCalendarDate from '~/components/ui/calendar/BlankCalendarDate';
import CalendarDate from '~/components/ui/calendar/CalendarDate';
import {
  calculateBlankDaysFromEnd,
  calculateBlankDaysFromStart,
  groupCategoryToColorCode,
  groupEventsByDay,
  getDaysOfMonth,
} from '~/components/ui/calendar/util';
import { WEEK_DATES } from '~/components/ui/calendar/constants';
import { useContext, useEffect, useState } from 'react';
import { DayOfMonth } from '~/components/ui/calendar/models';
import { DateTime } from 'luxon';
import EventForm from '~/components/ui/calendar/EventForm';
import SidePanelContext from '~/store/side-panel-context';
import DemoModeContext from '~/store/demo-mode-context';

interface CalendarProps {
  date: DateTime; // The date we will extract the month of to display
  events: App.Event[]; // Events that will be displayed on the Calendar
  categories: App.Category[];
}

export default function Calendar({ date, events, categories }: CalendarProps) {
  const context = useContext(SidePanelContext);
  const { isDemoMode } = useContext(DemoModeContext);
  const [focusDate, setFocusDate] = useState<DateTime>(date);
  const today = DateTime.now();
  const [dates, setDates] = useState<DayOfMonth[]>([]);
  const [blankStartDays, setBlankStartDays] = useState<number>(0);
  const [blankEndDays, setBlankEndDays] = useState<number>(0);
  const [eventsByDay, setEventsByDay] = useState<Map<number, App.Event[]>>(
    new Map()
  );
  const [calendarEvents, setCalendarEvents] = useState<App.Event[]>(events);
  const [loadingNewEvents, setLoadingNewEvents] = useState<boolean>(false);
  const [showAddEventModal, setShowAddEventModal] = useState<boolean>(false);

  useEffect(() => {
    setCalendarEvents(events);
    setFocusDate(date);
  }, [events, date]);

  useEffect(() => {
    const newDates = getDaysOfMonth(focusDate);
    setDates(newDates);
    setBlankStartDays(calculateBlankDaysFromStart(newDates[0].date));
    setBlankEndDays(
      calculateBlankDaysFromEnd(newDates[newDates.length - 1].date)
    );
    const newEventsByDay = groupEventsByDay(
      newDates.map((d) => d.dayNumber),
      calendarEvents
    );
    setEventsByDay(newEventsByDay);
  }, [focusDate, calendarEvents]);

  function formatMonth(date: DateTime) {
    return date.toFormat('LLLL yyyy');
  }

  async function nextMonth(e: any) {
    e.preventDefault();
    const nextMonth = focusDate.plus({ month: 1 });

    // Fetch events
    // const result = await fetchEvents(
    //   nextMonth.startOf('month'),
    //   nextMonth.endOf('month')
    // );
    // if (result?.items) {
    //   setCalendarEvents(result.items);
    //   setFocusDate(nextMonth);
    // }
  }

  async function previousMonth(e: any) {
    e.preventDefault();
    const lastMonth = focusDate.minus({ month: 1 });
    // Fetch events
    // const result = await fetchEvents(
    //   lastMonth.startOf('month'),
    //   lastMonth.endOf('month')
    // );
    // if (result?.items) {
    //   setCalendarEvents(result.items);
    //   setFocusDate(lastMonth);
    // }
  }

  function isCalendarDateToday(dayNumber: number) {
    return (
      today.day === dayNumber &&
      focusDate.month === today.month &&
      focusDate.year === today.year
    );
  }

  function toggleAddEventModal(e: any) {
    e.preventDefault();
    context.setComponent(<EventForm categories={categories} />);
  }

  function onEventClick(e: App.Event) {
    context.setComponent(
      <EventForm categories={categories} existingEvent={e} />
    );
  }

  const colorsByCategory = groupCategoryToColorCode(categories);

  return (
    <main className='container mx-auto p-4 animate-fadeInBottom'>
      <header className='w-full bg-primary-200 rounded-t-2xl'>
        <div className='flex flex-col p-2'>
          <div className='flex flex-row items-center justify-between'>
            <h1 className='font-bold text-primary-700'>
              {formatMonth(focusDate)}
            </h1>
            <AppButton color='green' onClick={toggleAddEventModal}>
              Add Event
            </AppButton>
            <div className='flex flex-row space-x-2'>
              <AppButton disabled={loadingNewEvents} onClick={previousMonth}>
                Previous
              </AppButton>
              <AppButton disabled={loadingNewEvents} onClick={nextMonth}>
                Next
              </AppButton>
            </div>
          </div>
        </div>
        <div className='bg-primary-100 grid grid-cols-7 invisible sm:visible'>
          {WEEK_DATES.map((weekDate) => (
            <DayOfWeekHeader key={weekDate}>{weekDate}</DayOfWeekHeader>
          ))}
        </div>
      </header>
      <section className='bg-transparent h-full grid grid-cols-1 sm:grid-cols-7'>
        {Array.from({ length: blankStartDays }, (_, index) => index).map(
          (value) => (
            <BlankCalendarDate key={value} />
          )
        )}
        {dates.map((d) => (
          <CalendarDate
            key={d.dayNumber}
            dayNumber={d.dayNumber}
            colorCodeByCategory={colorsByCategory}
            isToday={isCalendarDateToday(d.dayNumber)}
            last={d.date === 'Saturday'}
            events={eventsByDay.get(d.dayNumber) || []}
            onEventClick={onEventClick}
          />
        ))}
        {Array.from({ length: blankEndDays }, (_, index) => index).map(
          (value) => (
            <BlankCalendarDate key={value} last={value === blankEndDays - 1} />
          )
        )}
      </section>
    </main>
  );
}
