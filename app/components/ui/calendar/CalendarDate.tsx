import CalendarEventItem from '~/components/ui/calendar/CalendarEventItem';
import { useEffect, useState } from 'react';

interface CalendarDateProps {
  dayNumber: number;
  events: App.Event[];
  colorCodeByCategory: Map<string, string>;
  isToday?: boolean;
  last?: boolean;
  onEventClick: (event: App.Event) => void;
}

export default function CalendarDate({
  dayNumber,
  last,
  isToday,
  colorCodeByCategory,
  events,
  onEventClick,
}: CalendarDateProps) {
  const [visibleEvents, setVisibleEvents] = useState<App.Event[]>([]);
  const [showLessVisible, setShowLessVisible] = useState<boolean>(false);
  const lastStyle = last ? 'sm:border-r-2' : 'sm:border-r-0';
  const todayHeaderStyle = isToday ? 'text-yellow-200' : '';
  const todayCardStyle = isToday
    ? 'border-yellow-200 sm:border-t-2 sm:border-r-2'
    : 'border-primary-100 ';

  function populateVisibleEvents(events: App.Event[]) {
    if (events.length > 3) {
      const eventsToShow = [];
      for (let i = 0; i <= 2; i++) {
        eventsToShow.push(events[i]);
      }
      setVisibleEvents(eventsToShow);
    } else {
      setVisibleEvents(events);
    }
  }

  useEffect(() => {
    populateVisibleEvents(events);
  }, [events]);

  function showMoreEvents() {
    setVisibleEvents(events);
    setShowLessVisible(true);
  }

  function showLessEvents() {
    populateVisibleEvents(events);
    setShowLessVisible(false);
  }

  return (
    <div
      className={`flex flex-col p-4 sm:border-l-2 sm:border-b-2 sm:border-t-0 border-2 min-h-36 max-h-36 overflow-auto ${lastStyle} ${todayCardStyle}`}
    >
      <header>
        <h1
          className={`text-lg font-bold text-primary-100 ${todayHeaderStyle}`}
        >
          {dayNumber}
        </h1>
        {showLessVisible && (
          <button
            className='text-primary-50 hover:text-primary-200'
            onClick={showLessEvents}
          >
            Show Less...
          </button>
        )}
      </header>
      <section className='flex-1'>
        <ul className='list-none'>
          {visibleEvents.map((e) => (
            <CalendarEventItem
              key={e.id}
              event={e}
              colorCategory={colorCodeByCategory.get(e.categoryId) || 'ONE'}
              onClick={() => onEventClick(e)}
            />
          ))}
          {visibleEvents.length !== events.length && (
            <li className='text-primary-50 hover:cursor-pointer hover:text-primary-200'>
              <button onClick={showMoreEvents} className='w-full h-full'>
                Show More...
              </button>
            </li>
          )}
        </ul>
      </section>
    </div>
  );
}
