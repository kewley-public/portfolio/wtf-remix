import useZodForm from '~/hooks/useZodForm';
import { EventSchema } from '~/models/event';
import AppSelection from '~/components/ui/form/AppSelection';
import { DateTime } from 'luxon';
import AppInput from '~/components/ui/form/AppInput';
import AppButton from '~/components/ui/AppButton';
import AppDatePicker from '~/components/ui/form/AppDatePicker';
import * as React from 'react';
import { useContext } from 'react';
import SidePanelContext from '~/store/side-panel-context';

interface Props {
  categories: App.Category[];
  existingEvent?: App.Event;
}

async function saveEvent(formData: FormData) {
  //TODO
  return {};
}

export default function EventForm({ categories, existingEvent }: Props) {
  const { closePanel } = useContext(SidePanelContext);

  const initialValues = existingEvent
    ? { ...existingEvent }
    : {
        description: '',
        amount: 0,
        categoryId: categories.length ? categories[0].id : null,
        date: DateTime.now().toJSDate(),
      };
  const titleText = existingEvent
    ? `Edit ${existingEvent.description}`
    : 'Add Event';
  const saveButtonText = existingEvent ? 'Save' : 'Add';

  const formik = useZodForm(initialValues, EventSchema);

  function onDateChange(date: Date, event: React.SyntheticEvent<any>) {
    console.info(date);
    formik.setFieldValue('date', date);
  }

  return (
    <form onSubmit={formik.handleSubmit} className='w-full p-4'>
      <div className='text-primary-50 text-2xl text-center font-bold'>
        {titleText}
      </div>
      <AppSelection
        label='Category'
        id='categoryId'
        name='categoryId'
        items={categories
          .filter((c) => c.id != null)
          .map((c) => ({
            id: c.id!!,
            display: c.description,
          }))}
        required
        value={formik.values.categoryId}
        onChange={formik.handleChange}
        error={formik.errors.categoryId}
      />

      <AppInput
        label='Description'
        id='description'
        name='description'
        required
        value={formik.values.description}
        onChange={formik.handleChange}
        error={formik.errors.description}
      />

      <AppInput
        label='Amount'
        id='amount'
        name='amount'
        type='number'
        required
        min={0}
        value={formik.values.amount}
        onChange={formik.handleChange}
        error={formik.errors.amount}
      />

      <AppDatePicker
        id='date'
        name='date'
        label='Event Date'
        required
        selected={formik.values.date}
        onChange={onDateChange}
      />

      <div className='flex justify-between items-center mt-5'>
        <AppButton
          type='submit'
          full
          color='green'
          className='mr-2'
          disabled={formik.isSubmitting || !formik.isValid || !formik.dirty}
        >
          {saveButtonText}
        </AppButton>

        <AppButton
          onClick={closePanel}
          color='white'
          className='ml-2'
          full
          disabled={formik.isSubmitting}
          outline
        >
          Cancel
        </AppButton>
      </div>
    </form>
  );
}
