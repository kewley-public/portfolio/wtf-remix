import { DayOfMonth } from '~/components/ui/calendar/models';
import { DateTime } from 'luxon';

export function calculateBlankDaysFromStart(startDay: string): number {
  return (
    {
      Sunday: 0,
      Monday: 1,
      Tuesday: 2,
      Wednesday: 3,
      Thursday: 4,
      Friday: 5,
      Saturday: 6,
    }[startDay] || 0
  );
}

export function calculateBlankDaysFromEnd(endDay: string): number {
  return (
    {
      Sunday: 6,
      Monday: 5,
      Tuesday: 4,
      Wednesday: 3,
      Thursday: 2,
      Friday: 1,
      Saturday: 0,
    }[endDay] || 0
  );
}

export function groupEventsByDay(
  dates: number[],
  events: App.Event[]
): Map<number, App.Event[]> {
  const grouping: Map<number, App.Event[]> = new Map();
  dates.forEach((value) => {
    grouping.set(value, []);
  });

  events.forEach((e) => {
    const date = DateTime.fromJSDate(new Date(e.date)).day;
    if (grouping.has(date)) {
      grouping.get(date)?.push(e);
    }
  });

  return grouping;
}

export function groupCategoryToColorCode(
  categories: App.Category[]
): Map<string, string> {
  const grouping: Map<string, string> = new Map();
  categories.forEach((c) => grouping.set(c.id!!, c.colorCodeCategory));
  return grouping;
}

export function getDaysOfMonth(date: DateTime): DayOfMonth[] {
  const daysInMonth = date.daysInMonth;
  const daysArray = [];

  if (daysInMonth == null) {
    return [];
  }

  for (let day = 1; day <= daysInMonth; day++) {
    const dayDate = date.set({ day });
    daysArray.push({
      dayNumber: day,
      date: dayDate.toFormat('cccc'), // 'cccc' format for full weekday name
    });
  }

  return daysArray;
}
