import { ReactNode } from 'react';

interface TableItemBase {
  id?: string | null;

  [key: string]: any; // index signature
}

interface Props<T extends TableItemBase> {
  onSelect?: (item: T) => void;
  striped?: boolean;
  className?: string;
  tableHeaderClassName?: string;
  tableDataClassName?: string;
  filteredHeaders?: string[];
  formattedHeaders?: { [key in keyof T]?: () => string };
  formatters?: { [key in keyof T]?: (value: T[key]) => ReactNode };
  title?: string;
  items: T[];
}

export default function Table<T extends TableItemBase>({
  items,
  className,
  tableHeaderClassName,
  tableDataClassName,
  title,
  onSelect,
  striped = false,
  filteredHeaders = [],
  formattedHeaders = {},
  formatters = {},
}: Props<T>) {
  // Header extraction and item preparation remains the same
  const headers = new Set<string>();
  const tableItems: T[] = [];
  items.forEach((item: T) => {
    const tableItem = {} as T;
    for (const [key, value] of Object.entries(item)) {
      const typedKey = key as keyof T;
      tableItem[typedKey] =
        formatters && formatters[typedKey]
          ? formatters[typedKey]!(value)
          : value;

      const headerKey =
        formattedHeaders && formattedHeaders[typedKey]
          ? formattedHeaders[typedKey]!()
          : key;
      headers.add(headerKey);
    }
    tableItems.push(tableItem);
  });

  const filteredColumnIndices = new Set();
  if (tableItems.length && filteredHeaders?.length) {
    Object.entries(tableItems[0]).map(([key], index) => {
      if (filteredHeaders.includes(key)) {
        filteredColumnIndices.add(index);
      }
    });
  }

  const selectableStyle = onSelect
    ? 'hover:bg-primary-200 hover:text-white hover:cursor-pointer'
    : '';

  function handleTableRowSelection(item: T) {
    if (onSelect) {
      onSelect(item);
    }
  }

  return (
    <div className='container mx-auto px-2 sm:px-4 lg:px-6'>
      <div className='mt-4'>
        <div className='align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-primary-700'>
          {title && (
            <div className='px-6 py-3 border border-primary-300 bg-primary-300 text-left text-xl leading-4 font-medium text-white uppercase tracking-wider'>
              {title}
            </div>
          )}
          {headers.size > 0 && (
            <table className={`min-w-full border-collapse ${className}`}>
              <thead>
                <tr className='bg-primary-600'>
                  {Array.from(headers)
                    .filter((_, index) => !filteredColumnIndices.has(index))
                    .map((header) => (
                      <th
                        key={header}
                        className={`px-6 py-3 border-b border-primary-800 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider ${tableHeaderClassName}`}
                      >
                        {header}
                      </th>
                    ))}
                </tr>
              </thead>
              <tbody>
                {tableItems.map((item, index) => (
                  <tr
                    onClick={() => handleTableRowSelection(item)}
                    key={index}
                    className={`text-primary-700 last:border-none border-b border-primary-800 ${selectableStyle} 
                    ${
                      striped && index % 2 === 0
                        ? 'bg-primary-50'
                        : 'bg-primary-100'
                    }
                  }`}
                  >
                    {Object.entries(item)
                      .filter((_, index) => !filteredColumnIndices.has(index))
                      .map(([key, value]) => (
                        <td
                          key={key}
                          className={`px-6 py-4 whitespace-no-wrap ${tableDataClassName}`}
                        >
                          {value}
                        </td>
                      ))}
                  </tr>
                ))}
              </tbody>
            </table>
          )}
          {headers.size === 0 && (
            <div className='min-w-full bg-primary-100 p-5'>
              <h1 className='text-xl text-center text-primary-500 font-bold'>
                No Results
              </h1>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
