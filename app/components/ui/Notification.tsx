import { useContext } from 'react';

import NotificationContext from '~/store/notification-context';
import ReactDOM from 'react-dom';

function Notification() {
  const notificationCtx = useContext(NotificationContext);

  if (notificationCtx?.notification == null) {
    return null;
  }

  const { title, status, message } = notificationCtx.notification;

  let statusClasses = '';

  if (status === 'success') {
    statusClasses = 'bg-green-400';
  }

  if (status === 'error' || status === 'danger') {
    statusClasses = 'bg-red-400';
  }

  if (status === 'warn' || status === 'warning') {
    statusClasses = 'bg-yellow-400';
  }

  if (status === 'pending') {
    statusClasses = 'bg-primary-400';
  }

  const activeClasses = `z-50 animate-fadeInBottom fixed bottom-5 left-5 h-[5rem] w-100 text-white flex flex-col justify-between items-center p-2 shadow-2xl rounded ${statusClasses}`;

  return ReactDOM.createPortal(
    <button
      className={activeClasses}
      onClick={notificationCtx.hideNotification}
    >
      <h2 className='m-0 text-xl'>{title}</h2>
      <p>{message}</p>
    </button>,
    // @ts-expect-error Know what I'm doing
    document.getElementById('notifications') // Target container for the portal
  );
}

export default Notification;
