import { HTMLProps } from 'react';
import { FormikErrors } from 'formik';
import { parseErrorMessage } from '~/lib/util';
import { LinksFunction } from '@remix-run/node';

import appInputStyles from '~/components/ui/form/app-input.css';

export const links: LinksFunction = () => [
  { rel: 'stylesheet', href: appInputStyles },
];

interface Props extends HTMLProps<HTMLInputElement> {
  color?: 'blue' | 'red' | 'gray';
  error?:
    | string[]
    | string
    | null
    | undefined
    | FormikErrors<any>
    | FormikErrors<any>[];
}

export default function AppInput({
  htmlFor,
  id,
  label,
  color = 'blue',
  error,
  ...rest
}: Props) {
  const webKitAutoFills = {
    blue: 'form-input-blue',
    red: 'form-input-red',
    gray: 'form-input-red',
  }[color];

  const inputClass = {
    blue: 'text-primary-500 bg-primary-50 border-primary-200 focus:border-primary-500 focus:ring-primary-200 selection:bg-primary-500',
    red: 'text-red-500 bg-red-50 border-red-200 focus:border-red-500 focus:ring-red-200 selection:bg-red-500',
    gray: 'text-slate-500 bg-slate-50 border-slate-200 focus:border-slate-500 focus:ring-slate-200 selection:bg-slate-500',
  }[color];

  const baseClass =
    'w-full rounded-lg border-2 focus:outline-none focus:ring-0 focus:ring-opacity-50 selection:text-white shadow-sm p-2';
  const inputStyle = `${baseClass} ${inputClass}`;

  const errorMessage = parseErrorMessage(error);

  return (
    <div className='flex flex-col my-3 w-full'>
      <label
        htmlFor={htmlFor ? htmlFor : id}
        className='font-bold text-sm text-white my-1'
      >
        {label}
      </label>
      <input id={id} {...rest} className={inputStyle} />
      {errorMessage && <p className='text-red-500 text-sm'>{errorMessage}</p>}
    </div>
  );
}
