import { HTMLProps } from 'react';
import { FormikErrors } from 'formik';
import { parseErrorMessage } from '~/lib/util';

interface SelectableItem {
  id: string;
  display: string;
}

interface Props extends HTMLProps<HTMLSelectElement> {
  items: SelectableItem[];
  error?:
    | string[]
    | string
    | null
    | undefined
    | FormikErrors<any>
    | FormikErrors<any>[];
}

export default function AppSelection({
  items,
  id,
  htmlFor,
  label,
  error,
  ...rest
}: Props) {
  const errorMessage = parseErrorMessage(error);
  return (
    <div className='flex flex-col my-3 w-full'>
      <label
        htmlFor={htmlFor ? htmlFor : id}
        className='font-bold text-sm text-white my-1'
      >
        {label}
      </label>
      <select
        id={htmlFor ? htmlFor : id}
        {...rest}
        className='border border-gray-300 rounded px-3 py-2 focus:outline-none focus:ring-2 focus:ring-primary-500 text-primary-600 bg-primary-50'
      >
        {items.map((item) => (
          <option key={item.id} value={item.id}>
            {item.display}
          </option>
        ))}
      </select>
      {errorMessage && <p className='text-red-500 text-sm'>{errorMessage}</p>}
    </div>
  );
}
