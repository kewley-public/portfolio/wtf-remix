import { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren {
  type?: 'info' | 'success' | 'warn' | 'danger';
  position?: 't' | 'r' | 'l' | 'b' | 'tr' | 'tl' | 'br' | 'bl';
  onClick?: () => void;
}

export default function FAB({
  type = 'info',
  position = 'tl',
  children,
  onClick,
}: Props) {
  const positionClass: Record<string, string> = {
    t: 'top-0 left-1/2 transform -translate-x-1/2', // Top center
    r: 'right-0 top-1/2 transform -translate-y-1/2', // Right center
    l: 'left-0 top-1/2 transform -translate-y-1/2', // Left center
    b: 'bottom-0 left-1/2 transform -translate-x-1/2', // Bottom center
    tr: 'top-0 right-0', // Top right
    tl: 'top-0 left-0', // Top left
    br: 'bottom-0 right-0', // Bottom right
    bl: 'bottom-0 left-0', // Bottom left
  };
  const finalizedPositionClass = positionClass[position] || positionClass.tl;

  const typeClass: Record<string, string> = {
    info: 'hover:bg-primary-50 hover:text-primary-400 bg-primary-400 text-white',
    success: 'hover:bg-green-400 hover:text-green-50 bg-green-600 text-white',
    warn: 'hover:bg-yellow-50 hover:text-yellow-400 bg-yellow-400 text-white',
    danger: 'hover:bg-red-50 hover:text-red-400 bg-red-400 text-white',
  };
  const finalizedTypeClass = typeClass[type] || typeClass.info;

  return (
    <div className={`absolute ${finalizedPositionClass}`}>
      <button
        className={`rounded-full px-6 py-4 ${finalizedTypeClass}`}
        onClick={onClick}
      >
        {children}
      </button>
    </div>
  );
}
