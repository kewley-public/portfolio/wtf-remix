import { ButtonHTMLAttributes } from 'react';
import LoadingSpinner from '~/components/ui/LoadingSpinner';
import { Link } from '@remix-run/react';

interface Props extends ButtonHTMLAttributes<any> {
  to?: string;
  loading?: boolean;
  outline?: boolean;
  full?: boolean;
  circular?: boolean;
  color?: 'white' | 'blue' | 'red' | 'gray' | 'primary' | 'green' | 'yellow';
}

export default function AppButton({
  to,
  children,
  loading,
  disabled,
  className,
  outline,
  full = false,
  circular = false,
  color = 'primary',
  ...rest
}: Props) {
  const colorVariants: Record<string, string> = {
    white: 'text-black bg-white hover:bg-slate-200',
    primary: 'text-primary-50 bg-primary-500 hover:bg-primary-600',
    blue: 'text-blue-50 bg-blue-500 hover:bg-blue-600',
    red: 'text-red-50 bg-red-500 hover:bg-red-600',
    gray: 'text-slate-50, bg-slate-500 hover:bg-slate-600',
    green: 'text-green-50 bg-green-500 hover:bg-green-600',
    yellow: 'text-yellow-50 bg-yellow-500 hover:bg-yellow-600',
  };
  const outlineVariants: Record<string, string> = {
    white:
      'border-2 border-white text-white hover:bg-white hover:text-slate-500',
    primary:
      'text-primary-500 border-2 border-primary-500 hover:bg-primary-500 hover:text-white',
    blue: 'text-blue-800 border-2 border-blue-800 hover:bg-blue-800 hover:text-white',
    red: 'text-red-500 border-2 border-red-500 hover:bg-red-500 hover:text-white',
    gray: 'text-slate-500 border-2 border-slate-500 hover:bg-slate-500 hover:text-white',
    green:
      'text-green-800 border-2 border-green-800 hover:bg-green-800 hover:text-white',
    yellow:
      'text-yellow-500 border-2 border-yellow-500 hover:bg-yellow-500 hover:text-white',
  };
  const disabledVariants: Record<string, string> = {
    white: 'border-2 bg-blue-600 text-white opacity-80',
    primary:
      'border-2 border-primary-800 bg-primary-800 text-primary-100 opacity-80',
    blue: 'border-2 border-blue-800 bg-blue-800 text-blue-100 opacity-80',
    red: 'border-2 border-red-800 bg-red-800 text-red-100 opacity-80',
    gray: 'border-2 border-slate-800 bg-slate-800 text-slate-100 opacity-80',
    green: 'border-2 border-green-800 bg-green-800 text-green-100 opacity-80',
    yellow:
      'border-2 border-yellow-800 bg-yellow-800 text-yellow-100 opacity-80',
  };
  const baseStyle =
    'flex flex-row flex-wrap items-center justify-center px-3 py-2 rounded';
  const enabledBaseStyle = outline
    ? outlineVariants[color]
    : colorVariants[color];

  const rootButtonStyle = `
    ${circular ? 'rounded-full' : baseStyle}
    ${full ? 'w-full' : ''}
    ${disabled ? disabledVariants[color] : enabledBaseStyle}
    ${className}
  `.trim();

  const content = (
    <>
      {loading && (
        <div className='mr-3'>
          <LoadingSpinner />
        </div>
      )}
      {children}
    </>
  );

  if (to) {
    return (
      <Link to={to} {...rest} className={rootButtonStyle}>
        {content}
      </Link>
    );
  }

  return (
    <button disabled={disabled} {...rest} className={rootButtonStyle}>
      {content}
    </button>
  );
}
