import Table from '~/components/ui/Table';
import AppButton from '~/components/ui/AppButton';
import { useNavigate } from '@remix-run/react';

interface Props {
  categories: App.Category[];
}

export default function CategoriesTable({ categories }: Props) {
  const navigate = useNavigate();

  function onAddClickHandler() {
    navigate('/settings/categories/add');
  }

  return (
    <main className='flex flex-col'>
      <section>
        <Table
          filteredHeaders={['id', 'fundId']}
          items={categories}
          formatters={{}}
          formattedHeaders={{}}
          title='Categories'
        />
      </section>

      <section className='flex items-center justify-center mt-5 mx-7'>
        <AppButton color='white' outline full onClick={onAddClickHandler}>
          Add
        </AppButton>
      </section>
    </main>
  );
}
