import { retrievePaginatedResults } from '~/api/api.server';
import { EventSchema } from '~/models';
import { DateTime } from 'luxon';

export async function getEvents(
  accessToken: string,
  start: DateTime,
  end: DateTime
) {
  return await retrievePaginatedResults('events', accessToken, EventSchema, {
    start: start.toISO(),
    end: end.toISO(),
  });
}
