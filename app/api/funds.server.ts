import { retrieveListResults } from '~/api/api.server';
import { FundSchema } from '~/models';

export async function getFunds(
  accessToken: string
): Promise<App.Fund[] | undefined> {
  return await retrieveListResults<App.Fund>('funds', accessToken, FundSchema);
}
