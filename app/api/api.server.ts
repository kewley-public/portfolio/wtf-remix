import { z } from 'zod';
import CONFIG from '~/lib/config';

/**
 * Retrieves as single record from the server.
 *
 * @param endpoint - endpoint from the server
 * @param itemId - the item identifier
 * @param accessToken - user JWT token provided by the server
 * @param zodSchema - validation schema for parsing the objects back
 */
export async function retrieveItem<T>(
  endpoint: string,
  itemId: string,
  accessToken: string,
  zodSchema: z.ZodSchema
): Promise<T | undefined> {
  const url = new URL(`${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}/${itemId}`);
  try {
    const response = await fetch(url.toString(), {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    if (response.ok) {
      const rawResponse = await response.json();
      return zodSchema.parse(rawResponse);
    } else {
      console.error(
        `Error retrieving record status[${response.status}] - text[${response.statusText}]`
      );
    }
  } catch (e: any) {
    console.error(e);
  }
}

/**
 * Retrieves {@link App.Paginated} data from the server.
 *
 * Usage:
 * ```ts
 *   return await retrievePaginatedResults<App.Expense>(
 *     'expenses',
 *     accessToken,
 *     ExpenseSchema,
 *     {
 *       start: start.toISO()!!,
 *       end: end.toISO()!!,
 *     }
 *   );
 * ```
 * @param endpoint - endpoint from the server
 * @param accessToken - user JWT token provided by the server
 * @param zodSchema - validation schema for parsing the objects back
 * @param queryParams - optional query parameters
 */
export async function retrievePaginatedResults<T>(
  endpoint: string,
  accessToken: string,
  zodSchema: z.ZodSchema,
  queryParams: Record<string, string | null | undefined> = {}
): Promise<App.Paginated<T> | undefined> {
  const url = new URL(`${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}`);
  Object.keys(queryParams).forEach((key: string) => {
    const queryValue = queryParams[key];
    if (queryValue) {
      url.searchParams.set(key, queryValue);
    }
  });
  try {
    const response = await fetch(url.toString(), {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    if (response.ok) {
      const rawResults = await response.json();
      const items = rawResults.items.map((item: any) => zodSchema.parse(item));
      return {
        items,
        lastEvaluatedKey: rawResults.lastEvaluatedKey,
      };
    } else {
      console.error(
        `Error retrieving paginated results status[${response.status}] - text[${response.statusText}]`
      );
    }
  } catch (e: any) {
    console.error(e);
  }
}

/**
 * Similar to {@link retrievePaginatedResults} but returns a raw list from the
 * server. Typically for items that are known to not need pagination or for all results.
 *
 * Usage:
 * ```ts
 *   return await retrieveListResults('categories', accessToken, CategorySchema);
 * ```
 * @param endpoint - endpoint from the server
 * @param accessToken - user JWT token provided by the server
 * @param zodSchema - validation schema for parsing the objects back
 * @param queryParams - optional query parameters
 */
export async function retrieveListResults<T>(
  endpoint: string,
  accessToken: string,
  zodSchema: z.ZodSchema,
  queryParams: Record<string, string> = {}
): Promise<T[] | undefined> {
  const url = new URL(`${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}`);
  Object.keys(queryParams).forEach((key: string) => {
    url.searchParams.set(key, queryParams[key]);
  });
  try {
    const response = await fetch(url.toString(), {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    if (response.ok) {
      const rawResults = await response.json();
      return rawResults.map((item: any) => zodSchema.parse(item));
    } else {
      console.error(
        `Error retrieving list results status[${response.status}] - text[${response.statusText}]`
      );
    }
  } catch (e: any) {
    console.error(e);
  }
}

/**
 * Sends a PUT/POST to the server to save an item on the server.
 *
 * @param endpoint - endpoint from the server
 * @param accessToken - user JWT token provided by the server
 * @param payload - json payload to send to the server
 * @param create - determines a POST/PUT (e.g. POST for create, PUT for update)
 * @param itemId - item identifier
 * @param responseSchema - Optional, validation schema for parsing the objects back
 */
export async function saveRecord<T>(
  endpoint: string,
  accessToken: string,
  payload: Record<string, any>,
  create: boolean,
  itemId: string | null = null,
  responseSchema: z.Schema | null = null
): Promise<T | string | undefined> {
  const url = new URL(`${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}`);
  try {
    const response = await fetch(create ? url : `${url}/${itemId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      method: create ? 'POST' : 'PUT',
      body: JSON.stringify(payload),
    });
    if (response.ok) {
      const contentType = response.headers.get('Content-Type');
      if (contentType && contentType.includes('application/json')) {
        if (responseSchema != null) {
          const rawResponse = await response.json();
          return responseSchema.parse(rawResponse);
        }
        return await response.json();
      }
      return await response.text();
    } else {
      console.error(
        `Error saving record status[${response.status}] - text[${response.statusText}]`
      );
    }
  } catch (e: any) {
    console.error(e);
  }
}

/**
 * Sends a DELETE request to the server.
 *
 * @param endpoint - endpoint from the server
 * @param accessToken - user JWT token provided by the server
 * @param itemId - item identifier
 */
export async function deleteRecord(
  endpoint: string,
  accessToken: string,
  itemId: string
): Promise<boolean> {
  const url = new URL(`${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}/${itemId}`);
  try {
    const response = await fetch(url.toString(), {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      method: 'DELETE',
    });
    if (!response.ok) {
      console.error(
        `Error saving record status[${response.status}] - text[${response.statusText}]`
      );
    }
    return response.ok;
  } catch (e: any) {
    console.error(e);
  }
  return false;
}
