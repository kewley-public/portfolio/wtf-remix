import { DateTime } from 'luxon';
import { ExpenseSchema } from '~/models';
import {
  deleteRecord,
  retrieveItem,
  retrievePaginatedResults,
  saveRecord,
} from '~/api/api.server';

export async function getExpense(
  expenseId: string,
  accessToken: string
): Promise<App.Expense | undefined> {
  return await retrieveItem<App.Expense>(
    'expenses',
    expenseId,
    accessToken,
    ExpenseSchema
  );
}

export async function getExpenses(
  accessToken: string,
  start: DateTime,
  end: DateTime
): Promise<App.Paginated<App.Expense> | undefined> {
  return await retrievePaginatedResults<App.Expense>(
    'expenses',
    accessToken,
    ExpenseSchema,
    {
      start: start.toISO()!!,
      end: end.toISO()!!,
    }
  );
}

export async function saveExpense(
  accessToken: string,
  payload: Record<string, any>,
  expenseId: string | null = null
) {
  return await saveRecord(
    'expenses',
    accessToken,
    payload,
    expenseId == null,
    expenseId
  );
}

export async function deleteExpense(accessToken: string, expenseId: string) {
  return await deleteRecord('expenses', accessToken, expenseId);
}
