import { CategorySchema } from '~/models';
import { retrieveListResults } from '~/api/api.server';

export async function getCategories(
  accessToken: string
): Promise<App.Category[] | undefined> {
  return await retrieveListResults('categories', accessToken, CategorySchema);
}
