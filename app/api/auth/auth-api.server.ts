import CONFIG from '~/lib/config';
import { parse, serialize } from 'cookie';
import { createCookieSessionStorage, redirect } from '@remix-run/node';
import { isSessionExpiringSoon } from '~/lib/util';

// The secret used for session encryption. Must be set in environment variables for security.
const SESSION_SECRET = process.env.SESSION_SECRET as string;

/**
 * Configures session storage using cookies.
 * Sessions are encrypted using a secret key and are configured with security and
 * usability settings appropriate for most applications.
 */
export const SESSION_STORAGE = createCookieSessionStorage({
  cookie: {
    // Use HTTPS in production for secure cookie transmission.
    secure: process.env.NODE_ENV === 'production',
    // The session secret key for encrypting the cookie.
    secrets: [SESSION_SECRET],
    // Use 'lax' for CSRF protection.
    sameSite: 'lax',
    // Set cookie lifespan to 30 days.
    maxAge: 30 * 24 * 60 * 60,
    // Restrict cookie access to HTTP(S) requests only.
    httpOnly: true,
  },
});

/**
 * Registers a new user using the provided form data.
 *
 * @param registrationForm - The user registration form data.
 * @returns The registration response or null if registration fails.
 */
export async function registerUser(
  registrationForm: App.UserAuthForm
): Promise<App.RegistrationResponse | null> {
  const payload = {
    email: registrationForm.email,
    googleOAuthId: registrationForm.googleOAuthId,
    userName: registrationForm.userName,
    password: registrationForm.password,
  };

  const payloadAsString = JSON.stringify(payload);

  try {
    const fetchResponse = await fetch(
      `${CONFIG.BASE_SERVER_API}/auth/v1/register-user`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: payloadAsString,
      }
    );

    if (!fetchResponse.ok) {
      console.error(
        'Unable to register user',
        fetchResponse.status,
        fetchResponse.statusText
      );
      throw new Error('Unable to register user');
    }

    return await fetchResponse.json();
  } catch (error) {
    console.error('Network error occurred', error);
    throw new Error('Network error occurred');
  }
}

/**
 * Verifies an ID token from an OAuth provider and creates a user if they do not exist.
 *
 * @param idToken - The ID token provided by the OAuth service.
 * @param provider - The name of the OAuth provider (e.g., "google").
 * @returns A promise that resolves to the authorized user response or null.
 */
export async function verifyIdTokenAndCreateUserIfNotExist(
  idToken: string,
  provider: string
): Promise<App.AuthorizedUserResponse | null> {
  const fetchResponse = await fetch(
    `${CONFIG.BASE_SERVER_API}/auth/v1/verify-id-token`,
    {
      method: 'POST',
      body: JSON.stringify({ idToken, provider: provider.toUpperCase() }),
      headers: {
        'Content-Type': 'application/json',
      },
    }
  );

  if (!fetchResponse.ok) {
    console.error(
      'Unable to verify oauth user',
      fetchResponse.status,
      fetchResponse.statusText
    );
    throw new Error('Unable to get user');
  }

  const authorizedResponse = await fetchResponse.json();

  return _addRefreshTokenCookie(authorizedResponse, fetchResponse);
}

/**
 * Authenticates a user based on their email and password.
 *
 * @param email - The user's email address.
 * @param password - The user's password.
 * @returns A promise that resolves to the authorized user response or null.
 */
export async function authorizeUser(
  email: string,
  password: string
): Promise<App.AuthorizedUserResponse | null> {
  const fetchResponse = await fetch(
    `${CONFIG.BASE_SERVER_API}/auth/v1/authorize-user`,
    {
      method: 'POST',
      body: JSON.stringify({
        email,
        password,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    }
  );

  if (!fetchResponse.ok) {
    console.error(
      'Unable to get user by email',
      fetchResponse.status,
      fetchResponse.statusText
    );
    throw new Error('Unable to get user');
  }

  const authorizedResponse = await fetchResponse.json();

  return _addRefreshTokenCookie(authorizedResponse, fetchResponse);
}

/**
 * Refreshes the user session based on the refresh token, if available.
 *
 * @param request - The incoming request object.
 * @param redirectTo - Optional. A URL to redirect to upon successful session refresh.
 * @returns A promise that resolves to a redirect response.
 */
export async function refreshSession(
  request: Request,
  redirectTo: string | null | undefined = null
) {
  const user = await _getUserFromSession(request);
  // Backup incase this was a triggered /refresh event
  const referer = request.headers.get('referer');
  if (user?.refreshToken) {
    const fetchResponse = await fetch(
      `${CONFIG.BASE_SERVER_API}/auth/v1/refresh-session`,
      {
        headers: {
          cookie: serialize('refreshToken', user.refreshToken),
        },
      }
    );

    if (!fetchResponse.ok) {
      console.error(
        'Unable to get user by email',
        fetchResponse.status,
        fetchResponse.statusText
      );
      throw new Error('Unable to get user');
    }
    const reAuthenticatedUser = await fetchResponse.json();
    const session = await SESSION_STORAGE.getSession(
      request.headers.get('Cookie')
    );
    session.set('user', {
      ...user,
      ...reAuthenticatedUser,
    });
    const sessionCookie = await SESSION_STORAGE.commitSession(session);
    return redirect(redirectTo || referer || '/', {
      headers: {
        'Set-Cookie': sessionCookie,
      },
    });
  }
  throw Error('No refresh token provided');
}

/**
 * Adds a refresh token to the authorized user response as a cookie.
 *
 * @param authorizedUser - The authorized user response object.
 * @param response - The fetch response from which to extract the refresh token cookie.
 * @returns The authorized user response with the refresh token included.
 */
function _addRefreshTokenCookie(
  authorizedUser: App.AuthorizedUserResponse,
  response: Response
): App.AuthorizedUserResponse {
  const apiCookie = response.headers.get('set-cookie');
  if (apiCookie) {
    const parsedCookie = parse(apiCookie);
    const [, cookieValue] = Object.entries(parsedCookie)[0];
    authorizedUser.refreshToken = cookieValue;
  }
  return authorizedUser;
}

/**
 * Retrieves the access token from the current user session.
 *
 * @param request - The incoming request object.
 * @returns A promise that resolves to the access token or null if not found.
 */
export async function getAccessTokenFromSession(
  request: Request
): Promise<null | App.AccessToken> {
  const user = await _getUserFromSession(request);
  if (user?.accessToken) {
    return {
      token: user.accessToken,
      expiresAt: user.expiresAt,
    };
  }
  return null;
}

/**
 * Retrieves the user from the request session
 *
 * @param request - The incoming request object.
 */
async function _getUserFromSession(
  request: Request
): Promise<App.AuthorizedUserResponse> {
  const session = await SESSION_STORAGE.getSession(
    request.headers.get('Cookie')
  );
  return session.get('user');
}

/**
 * Requires that the request has an associated user session that is not expiring soon.
 * Redirects to the login page if no session is found or if the session is expiring soon.
 *
 * @param request - The incoming request object.
 * @returns The access token string if a valid session exists.
 */
export async function requireUserSession(request: Request) {
  const accessToken = await getAccessTokenFromSession(request);
  if (accessToken == null) {
    throw redirect('/auth?mode=login');
  }
  if (isSessionExpiringSoon(accessToken.expiresAt)) {
    throw await refreshSession(request, request.url);
  }
  return accessToken.token;
}
