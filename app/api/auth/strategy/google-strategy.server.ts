import { verifyIdTokenAndCreateUserIfNotExist } from '~/api/auth/auth-api.server';
import { OAuth2Strategy } from 'remix-auth-oauth2';
import logger from '~/lib/logging';

const googleStrategy = new OAuth2Strategy(
  {
    authorizationURL: 'https://accounts.google.com/o/oauth2/v2/auth',
    tokenURL: 'https://oauth2.googleapis.com/token',
    clientID: process.env.GOOGLE_CLIENT_ID!!,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET!!,
    callbackURL: process.env.GOOGLE_CALLBACK!!,
    // Specify the scopes you need
    scope: 'openid email profile',
  },
  // The verify callback, where you handle the user information received from Google
  async ({ extraParams }) => {
    try {
      const user = await verifyIdTokenAndCreateUserIfNotExist(
        extraParams.id_token,
        'google'
      );
      logger.info('Authorized User', user?.id);
      return user;
    } catch (e: any) {
      logger.error('Error verifying user');
      logger.error(e.message);
      return null;
    }
  }
);

export default googleStrategy;
