import { Strategy } from 'remix-auth';
import { UserAuthFormSchema } from '~/models';
import { authorizeUser } from '~/api/auth/auth-api.server';
import logger from '~/lib/logging';

/**
 * Implements a custom authentication strategy using email and password.
 * This strategy verifies user credentials against a database and handles
 * both authentication success and failure scenarios.
 */
export class CredentialsStrategy extends Strategy<
  App.AuthorizedUserResponse,
  App.AuthorizedUserResponse
> {
  /**
   * Initializes a new instance of the CredentialsStrategy.
   *
   * @param {string} name - The name of the strategy, defaults to 'credentials'.
   * @param {Function} getUser - The function to authenticate a user, defaults to authorizeUser.
   */
  constructor(
    public name: string = 'credentials',
    private getUser: typeof authorizeUser = authorizeUser
  ) {
    super(verify);
  }

  /**
   * Authenticates a user based on email and password submitted through a form.
   * Validates the form data, attempts to authenticate the user, and handles
   * the outcome by either succeeding or failing the authentication process.
   *
   * @param {Request} request - The incoming request object.
   * @param sessionStorage - The session storage used to manage user sessions.
   * @param options - Additional options for the authentication process.
   * @returns A promise resolving to the authentication result.
   */
  async authenticate(request: Request, sessionStorage: any, options: any) {
    const formData = await request.formData();
    const email = formData.get('email');
    const password = formData.get('password');

    // Validates the form data using the UserAuthFormSchema.
    const verify = UserAuthFormSchema.safeParse({ email, password });

    // If validation fails, immediately return a failure.
    if (!verify.success) {
      return this.failure(
        'Invalid Credentials',
        request,
        sessionStorage,
        options,
        verify.error
      );
    }

    const verifiedResponse = verify.data;

    // Attempt to authenticate the user with the provided credentials.
    let user = null;
    try {
      user = await this.getUser(
        verifiedResponse.email,
        verifiedResponse.password
      );
    } catch (e: any) {
      logger.error(e);
      return this.failure('Server Error', request, sessionStorage, options, e);
    }

    // If authentication fails (user not found or invalid credentials), return a failure.
    if (!user) {
      return this.failure(
        'Could not log you in, please check the provided credentials.',
        request,
        sessionStorage,
        options
      );
    }

    // On successful authentication, return a success with the user data.
    return this.success(user, request, sessionStorage, options);
  }
}

/**
 * A placeholder verification function that simply returns the user object.
 * This could be extended to perform additional verification steps if needed.
 *
 * @param {App.AuthorizedUserResponse} user - The user object to verify.
 * @returns {Promise<App.AuthorizedUserResponse>} The verified user.
 */
async function verify(user: App.AuthorizedUserResponse) {
  return user;
}
