import { Authenticator } from 'remix-auth';
import { SESSION_STORAGE } from '~/api/auth/auth-api.server';
import googleStrategy from '~/api/auth/strategy/google-strategy.server';
import { CredentialsStrategy } from '~/api/auth/strategy/credentials-strategy.server';

/**
 * Configures and initializes the Authenticator for the application.
 *
 * The Authenticator is responsible for managing authentication strategies,
 * sessions, and user authentication flows in the application. It leverages
 * the SESSION_STORAGE for persisting session data across requests.
 *
 * Strategies:
 * - Google OAuth Strategy: Allows users to authenticate using their Google account.
 *   This strategy is defined in '~/api/auth/strategy/google-strategy.server' and
 *   handles the OAuth flow, including redirecting users to Google for authentication,
 *   and processing the callback with the user's information.
 *
 * - Credentials Strategy: Enables username and password authentication.
 *   Defined in '~/api/auth/strategy/credentials-strategy.server', it verifies user credentials
 *   against a stored database and authenticates the user if the credentials are valid.
 *
 * The use of multiple strategies allows for flexible authentication options within
 * the application, catering to different user preferences and requirements.
 *
 * Usage:
 * The Authenticator instance should be used in routes to authenticate requests,
 * manage login flows, and handle user sessions. It is configured to use the
 * SESSION_STORAGE for storing session data, ensuring that user sessions are
 * maintained securely and efficiently.
 */
const authenticator = new Authenticator(SESSION_STORAGE)
  .use(googleStrategy, 'google')
  .use(new CredentialsStrategy());

export default authenticator;
