import { DateTime } from 'luxon';

export const INITIAL_CATEGORIES: App.Category[] = [
  {
    id: '1',
    description: 'Bills',
    colorCodeCategory: 'ONE',
    fundId: '1',
  },
  {
    id: '2',
    description: 'Auto & Transport',
    colorCodeCategory: 'TWO',
    fundId: '2',
  },
  {
    id: '3',
    description: 'Out To Eat & Drinks"',
    colorCodeCategory: 'THREE',
    fundId: '3',
  },
  {
    id: '4',
    description: 'Entertainment & Rec',
    colorCodeCategory: 'FOUR',
    fundId: '4',
  },
  {
    id: '5',
    description: 'Other',
    colorCodeCategory: 'FIVE',
    fundId: '5',
  },
];

export const INITIAL_FUNDS: App.Fund[] = [
  {
    id: '1',
    description: 'Bills',
    categoryId: '1',
    amount: 100,
  },
  {
    id: '2',
    description: 'Auto & Transport',
    categoryId: '2',
    amount: 100,
  },
  {
    id: '3',
    description: 'Out To Eat & Drinks"',
    categoryId: '3',
    amount: 100,
  },
  {
    id: '4',
    description: 'Entertainment & Rec',
    categoryId: '4',
    amount: 100,
  },
  {
    id: '5',
    description: 'Other',
    categoryId: '5',
    amount: 100,
  },
];

export const INITIAL_EXPENSES: App.Expense[] = [
  {
    id: '1',
    amount: 62.99,
    description: 'Electric Bill',
    categoryId: '1',
    date: DateTime.now().toJSDate(),
    fundId: '1',
  },
];

export const INITIAL_EVENTS: App.Event[] = [
  {
    id: '1',
    amount: 20,
    description: 'Woot',
    categoryId: '1',
    date: DateTime.now().toJSDate(),
    recurrenceRule: null,
  },
];
