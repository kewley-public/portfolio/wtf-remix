import { LoaderFunction, LoaderFunctionArgs } from '@remix-run/node';
import { requireUserSession } from '~/api/auth/auth-api.server';
import { getCategories } from '~/api/categories.server';
import { defer, Outlet, useLoaderData } from '@remix-run/react';
import { getFunds } from '~/api/funds.server';
import FundsTable from '~/components/funds/FundsTable';
import CategoriesTable from '~/components/categories/CategoriesTable';
import AsyncComponent from '~/components/util/AsyncComponent';

export default function SettingsPage() {
  const { categories, funds } = useLoaderData<typeof loader>();
  return (
    <main>
      <Outlet />
      <section className='flex flex-col space-y-4 p-2'>
        <AsyncComponent
          resolve={Promise.all([funds, categories])}
          fallbackText='Loading funds...'
        >
          {([fundsResolved, categoriesResolved]) => (
            <section className='animate-fadeInTop'>
              <FundsTable
                funds={fundsResolved}
                categories={categoriesResolved}
              />
            </section>
          )}
        </AsyncComponent>

        <AsyncComponent
          resolve={categories}
          fallbackText='Loading categories...'
        >
          {(categoriesResolved) => (
            <section className='animate-fadeInBottom'>
              <CategoriesTable categories={categoriesResolved} />
            </section>
          )}
        </AsyncComponent>
      </section>
    </main>
  );
}

export const loader: LoaderFunction = async ({
  request,
}: LoaderFunctionArgs) => {
  const accessToken = await requireUserSession(request);
  return defer(
    {
      categories: getCategories(accessToken),
      funds: getFunds(accessToken),
    },
    {
      headers: { 'Cache-Control': 'max-age=3' },
    }
  );
};
