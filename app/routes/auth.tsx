import AuthForm from '~/components/auth/AuthForm';
import {
  json,
  LoaderFunction,
  LoaderFunctionArgs,
  redirect,
} from '@remix-run/node';
import { getAccessTokenFromSession } from '~/api/auth/auth-api.server';

export default function AuthPage() {
  return (
    <div className='animate-fadeInBottom'>
      <AuthForm />
    </div>
  );
}

export const loader: LoaderFunction = async ({
  request,
}: LoaderFunctionArgs) => {
  const accessToken = await getAccessTokenFromSession(request);
  if (accessToken) {
    return redirect('/wallet/expenses');
  }
  return json({});
};
