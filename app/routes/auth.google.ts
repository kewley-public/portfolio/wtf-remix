import { LoaderFunction, LoaderFunctionArgs, redirect } from '@remix-run/node';
import authenticator from '~/api/auth/auth-provider.server';

export const loader = () => redirect('/auth?mode=login');

export const action: LoaderFunction = async ({
  request,
}: LoaderFunctionArgs) => {
  return authenticator.authenticate('google', request);
};
