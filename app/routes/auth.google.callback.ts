import { LoaderFunction, LoaderFunctionArgs } from '@remix-run/node';
import authenticator from '~/api/auth/auth-provider.server';
import logger from '~/lib/logging';

export const loader: LoaderFunction = async ({
  request,
}: LoaderFunctionArgs) => {
  logger.info(`auth.google.callback: ${request.url}`);
  return authenticator.authenticate('google', request, {
    successRedirect: '/', // Redirect to home on success
    failureRedirect: '/auth?mode=login&failed=true&google=true', // Redirect to login on failure
  });
};
