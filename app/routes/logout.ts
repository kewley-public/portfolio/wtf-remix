import { ActionFunction, ActionFunctionArgs } from '@remix-run/node';
import authenticator from '~/api/auth/auth-provider.server';

export const action: ActionFunction = async ({
  request,
}: ActionFunctionArgs) => {
  return await authenticator.logout(request, {
    redirectTo: '/auth?mode=login',
  });
};
