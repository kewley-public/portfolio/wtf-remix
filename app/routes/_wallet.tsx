import { Outlet } from '@remix-run/react';
import WalletHeader from '~/components/navigation/WalletHeader';
import { LoaderFunction, LoaderFunctionArgs } from '@remix-run/node';
import { requireUserSession } from '~/api/auth/auth-api.server';

export default function WalletIndex() {
  return (
    <main className='flex flex-col space-y-4 p-2'>
      <WalletHeader />
      <Outlet />
    </main>
  );
}

export const loader: LoaderFunction = ({ request }: LoaderFunctionArgs) =>
  requireUserSession(request);
