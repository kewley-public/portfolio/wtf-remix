import { ActionFunction, ActionFunctionArgs, redirect } from '@remix-run/node';
import { refreshSession } from '~/api/auth/auth-api.server';

export const loader = () => redirect('/');

export const action: ActionFunction = async ({
  request,
}: ActionFunctionArgs) => {
  return await refreshSession(request);
};
