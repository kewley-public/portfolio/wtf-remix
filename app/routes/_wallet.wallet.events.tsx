import Calendar from '~/components/ui/calendar/Calendar';
import { LoaderFunction, LoaderFunctionArgs } from '@remix-run/node';
import { requireUserSession } from '~/api/auth/auth-api.server';
import { getCategories } from '~/api/categories.server';
import { defer, useLoaderData } from '@remix-run/react';
import { DateTime } from 'luxon';
import AsyncComponent from '~/components/util/AsyncComponent';

export default function EventsPage() {
  const { categories } = useLoaderData<typeof loader>();
  return (
    <section>
      <AsyncComponent resolve={categories} fallbackText='Loading Events...'>
        {(categoriesResolved) => (
          <Calendar
            date={DateTime.now()}
            events={[]}
            categories={categoriesResolved}
          />
        )}
      </AsyncComponent>
    </section>
  );
}

export const loader: LoaderFunction = async ({
  request,
}: LoaderFunctionArgs) => {
  const accessToken = await requireUserSession(request);
  return defer(
    { categories: getCategories(accessToken) },
    {
      headers: { 'Cache-Control': 'max-age=3' },
    }
  );
};
