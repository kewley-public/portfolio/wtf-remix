import { useLoaderData, useNavigate } from '@remix-run/react';
import SidePanel from '~/components/ui/SidePanel';
import ExpenseForm from '~/components/expenses/form/ExpenseForm';
import {
  ActionFunction,
  ActionFunctionArgs,
  json,
  LoaderFunction,
  LoaderFunctionArgs,
  redirect,
} from '@remix-run/node';
import { requireUserSession } from '~/api/auth/auth-api.server';
import { getCategories } from '~/api/categories.server';
import { DateTime } from 'luxon';
import { CreateExpenseSchema } from '~/models';
import { saveExpense } from '~/api/expeses.server';

export default function AddExpensePage() {
  const { categories } = useLoaderData<typeof loader>();
  const navigate = useNavigate();

  return (
    <SidePanel onOverlayClick={() => navigate(-1)}>
      <ExpenseForm categories={categories} onCancel={() => navigate(-1)} />
    </SidePanel>
  );
}

export const loader: LoaderFunction = async ({
  request,
}: LoaderFunctionArgs) => {
  const accessToken = await requireUserSession(request);
  const categories = await getCategories(accessToken);
  if (!categories) {
    throw new Response('Not Found', { status: 404 });
  }
  return json({ categories });
};

export const action: ActionFunction = async ({
  request,
}: ActionFunctionArgs) => {
  const accessToken = await requireUserSession(request);
  const formData = await request.formData();

  const dateValue = (formData.get('date') || DateTime.now().toISO()).toString();
  const expense = {
    description: formData.get('description'),
    amount: +(formData.get('amount') || '0'),
    categoryId: formData.get('categoryId'),
    date: new Date(dateValue),
  };
  const result = CreateExpenseSchema.safeParse(expense);

  if (!result.success) {
    console.error(result.error);
    throw new Response('Invalid request', { status: 400 });
  }

  await saveExpense(accessToken, result.data);
  return redirect('/wallet/expenses');
};
