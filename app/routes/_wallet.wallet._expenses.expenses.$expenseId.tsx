import {
  ActionFunction,
  ActionFunctionArgs,
  json,
  LoaderFunction,
  LoaderFunctionArgs,
  redirect,
} from '@remix-run/node';
import { deleteExpense, getExpense, saveExpense } from '~/api/expeses.server';
import { requireUserSession } from '~/api/auth/auth-api.server';
import { useLoaderData, useNavigate } from '@remix-run/react';
import invariant from 'tiny-invariant';
import { getCategories } from '~/api/categories.server';
import ExpenseForm from '~/components/expenses/form/ExpenseForm';
import SidePanel from '~/components/ui/SidePanel';
import { DateTime } from 'luxon';
import { ExpenseSchema } from '~/models';

export default function EditExpensePage() {
  const { expense, categories } = useLoaderData<typeof loader>();
  const navigate = useNavigate();

  return (
    <SidePanel onOverlayClick={() => navigate(-1)}>
      <ExpenseForm
        existingExpense={expense}
        categories={categories}
        onCancel={() => navigate(-1)}
      />
    </SidePanel>
  );
}

export const loader: LoaderFunction = async ({
  params,
  request,
}: LoaderFunctionArgs) => {
  invariant(params.expenseId, 'Missing expense id');
  const accessToken = await requireUserSession(request);
  const [expense, categories] = await Promise.all([
    getExpense(params.expenseId, accessToken),
    getCategories(accessToken),
  ]);
  if (!expense) {
    throw new Response('Not Found', { status: 404 });
  }
  return json({ expense, categories });
};

export const action: ActionFunction = async ({
  request,
  params,
}: ActionFunctionArgs) => {
  invariant(params.expenseId, 'Missing expense id');
  const accessToken = await requireUserSession(request);

  if (request.method === 'DELETE') {
    await deleteExpense(accessToken, params.expenseId);
  } else if (request.method === 'PUT') {
    const formData = await request.formData();

    const dateValue = (
      formData.get('date') || DateTime.now().toISO()
    ).toString();
    const expense = {
      id: formData.get('id'),
      description: formData.get('description'),
      amount: +(formData.get('amount') || '0'),
      categoryId: formData.get('categoryId'),
      date: new Date(dateValue),
      fundId: formData.get('fundId'),
    };
    const result = ExpenseSchema.safeParse(expense);

    if (!result.success) {
      console.error(result.error);
      throw new Response('Invalid request', { status: 400 });
    }

    await saveExpense(accessToken, result.data, params.expenseId);
  }
  return redirect('/wallet/expenses');
};
