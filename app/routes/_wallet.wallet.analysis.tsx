import { LoaderFunctionArgs } from '@remix-run/node';
import { requireUserSession } from '~/api/auth/auth-api.server';
import { DateTime } from 'luxon';
import { getExpenses } from '~/api/expeses.server';
import { getCategories } from '~/api/categories.server';
import { getFunds } from '~/api/funds.server';
import { defer, useLoaderData } from '@remix-run/react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  BarElement,
  DoughnutController,
} from 'chart.js/auto';
import ExpensesByCategory from '~/components/expenses/ExpensesByCategory';
import ExpensesByMonth from '~/components/expenses/ExpensesByMonth';
import Card from '~/components/ui/Card';
import ExpensesByFund from '~/components/expenses/ExpensesByFund';
import LoadingSpinner from '~/components/ui/LoadingSpinner';
import { ClientOnly } from '~/components/util/ClientOnly';
import AsyncComponent from '~/components/util/AsyncComponent';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  BarElement,
  DoughnutController
);

export default function Analysis() {
  const { funds, expenses, categories } = useLoaderData<typeof loader>();
  return (
    <section className='flex flex-col'>
      <div className='flex flex-wrap items-center justify-between mb-10'>
        <Card className='basis-1 min-w-[90vw] animate-fadeInLeft min-h-[60rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0'>
          <AsyncComponent
            resolve={Promise.all([funds, categories, expenses])}
            fallbackText='Loading Fund Chart...'
          >
            {([fundsResolved, categoriesResolved, expensesResolved]) => (
              <ClientOnly fallback={<Fallback text='Loading Fund Chart' />}>
                {() => (
                  <ExpensesByFund
                    funds={fundsResolved}
                    categories={categoriesResolved}
                    expenses={expensesResolved?.items}
                  />
                )}
              </ClientOnly>
            )}
          </AsyncComponent>
        </Card>
        <Card className='basis-1 min-w-[90vw] animate-fadeInRight min-h-[60rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0'>
          <AsyncComponent
            resolve={expenses}
            fallbackText='Loading expenses by month...'
          >
            {(expensesResolved) => (
              <ClientOnly fallback={<Fallback text='Loading Fund Chart' />}>
                {() => <ExpensesByMonth expenses={expensesResolved?.items} />}
              </ClientOnly>
            )}
          </AsyncComponent>
        </Card>
      </div>
      <Card>
        <AsyncComponent resolve={Promise.all([categories, expenses])}>
          {([categoriesResolved, expensesResolved]) => (
            <ClientOnly fallback={<Fallback text='Loading Fund Chart' />}>
              {() => (
                <ExpensesByCategory
                  categories={categoriesResolved}
                  expenses={expensesResolved?.items}
                />
              )}
            </ClientOnly>
          )}
        </AsyncComponent>
      </Card>
    </section>
  );
}

function Fallback({ text }: { text: string }) {
  return <LoadingSpinner>{text}</LoadingSpinner>;
}

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const accessToken = await requireUserSession(request);
  const start = DateTime.now().minus({ month: 2 }).startOf('month');
  const end = DateTime.now().endOf('month');
  return defer(
    {
      expenses: getExpenses(accessToken, start, end),
      categories: getCategories(accessToken),
      funds: getFunds(accessToken),
    },
    {
      headers: { 'Cache-Control': 'max-age=3' },
    }
  );
};
