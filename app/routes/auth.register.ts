import { ActionFunction, ActionFunctionArgs, redirect } from '@remix-run/node';
import { UserAuthFormSchema } from '~/models';
import { registerUser } from '~/api/auth/auth-api.server';

export const action: ActionFunction = async ({
  request,
}: ActionFunctionArgs) => {
  const formData = await request.formData();
  const credentials = Object.fromEntries(formData);
  const validUser = UserAuthFormSchema.safeParse(credentials);
  if (!validUser.success) {
    return {
      errors: validUser.error.errors,
    };
  }

  const userCredentials = validUser.data;
  try {
    const user = await registerUser({
      email: userCredentials.email,
      password: userCredentials.password,
    });
    if (user) {
      return redirect('/auth?mode=login');
    }
  } catch (e: any) {
    console.error(e);
    if (e.status === 401 || e.status === 422) {
      return {
        errors: [{ path: ['serverError'], message: e.message }],
      };
    }
  }
  return { errors: [{ path: ['serverError'], message: 'Unexpected error' }] };
};
