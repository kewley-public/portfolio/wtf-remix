import { ActionFunctionArgs } from '@remix-run/node';
import authenticator from '~/api/auth/auth-provider.server';

export const action = async ({ request }: ActionFunctionArgs) => {
  return await authenticator.authenticate('credentials', request, {
    successRedirect: '/',
    failureRedirect: '/auth?mode=login&failed=true',
  });
};
