import SidePanel from '~/components/ui/SidePanel';
import { useNavigate } from '@remix-run/react';

export default function AddCategoryPage() {
  const navigate = useNavigate();
  return (
    <SidePanel onOverlayClick={() => navigate(-1)}>
      <h1>Add Category</h1>
    </SidePanel>
  );
}
