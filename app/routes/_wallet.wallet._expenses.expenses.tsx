import { LoaderFunction, LoaderFunctionArgs } from '@remix-run/node';
import { Outlet, useLoaderData, defer } from '@remix-run/react';
import { requireUserSession } from '~/api/auth/auth-api.server';
import { getExpenses } from '~/api/expeses.server';
import { DateTime } from 'luxon';
import { getCategories } from '~/api/categories.server';
import ExpensesTable from '~/components/expenses/ExpensesTable';
import AsyncComponent from '~/components/util/AsyncComponent';

export default function WalletPage() {
  const { expenses, categories } = useLoaderData<typeof loader>();
  return (
    <main>
      <Outlet />
      <AsyncComponent
        fallbackText='Loading Expenses...'
        resolve={Promise.all([expenses, categories])}
      >
        {([expenses, categories]) => (
          <section className='animate-fadeInBottom'>
            <ExpensesTable expenses={expenses?.items} categories={categories} />
          </section>
        )}
      </AsyncComponent>
    </main>
  );
}

export const loader: LoaderFunction = async ({
  request,
}: LoaderFunctionArgs) => {
  const accessToken = await requireUserSession(request);
  const start = DateTime.now().minus({ month: 2 }).startOf('month');
  const end = DateTime.now().endOf('month');
  return defer(
    {
      expenses: getExpenses(accessToken, start, end),
      categories: getCategories(accessToken),
    },
    {
      headers: { 'Cache-Control': 'max-age=3' },
    }
  );
};
