import SidePanel from '~/components/ui/SidePanel';
import { useNavigate } from '@remix-run/react';

export default function EditFund() {
  const navigate = useNavigate();
  return (
    <SidePanel onOverlayClick={() => navigate(-1)}>
      <div>Edit Fund</div>
    </SidePanel>
  );
}
