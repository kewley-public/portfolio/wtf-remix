import { createContext, useState, PropsWithChildren, useEffect } from 'react';

interface NotificationState {
  notification: App.Notification | null | undefined;
  showNotification: (notification: App.Notification) => void;
  hideNotification: () => void;
}

const NotificationContext = createContext<NotificationState>({
  notification: null, // { title, message, status }
  showNotification: () => {},
  hideNotification: function () {},
});

export function NotificationContextProvider({ children }: PropsWithChildren) {
  const [notification, setNotification] = useState<
    App.Notification | null | undefined
  >(null);

  useEffect(() => {
    if (
      notification?.status === 'success' ||
      notification?.status === 'error'
    ) {
      const timer = setTimeout(() => setNotification(null), 3000);
      // This will run if useEffect is ever "re-called"
      return () => {
        clearTimeout(timer);
      };
    }
  }, [notification]);

  function showNotification(notification: App.Notification) {
    setNotification(notification);
  }

  function hideNotification() {
    setNotification(null);
  }

  const context: NotificationState = {
    notification,
    showNotification,
    hideNotification,
  };

  return (
    <NotificationContext.Provider value={context}>
      {children}
    </NotificationContext.Provider>
  );
}

export default NotificationContext;
