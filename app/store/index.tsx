import { PropsWithChildren } from 'react';
import { SidePanelContextProvider } from '~/store/side-panel-context';
import { NotificationContextProvider } from '~/store/notification-context';
import { AuthenticationContextProvider } from '~/store/authentication-context';

interface GlobalContextProviderProps extends PropsWithChildren {
  accessToken: App.AccessToken | null | undefined;
}

export default function GlobalContextProvider({
  children,
  accessToken,
}: GlobalContextProviderProps) {
  return (
    <AuthenticationContextProvider
      accessToken={accessToken?.token}
      expiresAt={accessToken?.expiresAt}
    >
      <NotificationContextProvider>
        <SidePanelContextProvider>{children}</SidePanelContextProvider>
      </NotificationContextProvider>
    </AuthenticationContextProvider>
  );
}
