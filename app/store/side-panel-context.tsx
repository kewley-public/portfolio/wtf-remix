import { createContext, PropsWithChildren, ReactNode, useState } from 'react';

interface SidePanelState {
  component: ReactNode | null;
  setComponent: (component: ReactNode | null) => void;
  closePanel: () => void;
}

const SidePanelContext = createContext<SidePanelState>({
  component: null,
  setComponent: () => {},
  closePanel: () => {},
});

export function SidePanelContextProvider({ children }: PropsWithChildren) {
  const [component, setComponent] = useState<ReactNode | null>(null);

  function closePanel() {
    setComponent(null);
  }

  return (
    <SidePanelContext.Provider value={{ component, setComponent, closePanel }}>
      {children}
    </SidePanelContext.Provider>
  );
}

export default SidePanelContext;
