import { createContext, PropsWithChildren, useState } from 'react';

interface AuthenticationState {
  accessToken: string | null | undefined;
  expiresAt: number | null | undefined;
  checkingSession: boolean;
  setCheckingSession: (val: boolean) => void;
}

interface AuthenticationProps extends PropsWithChildren {
  accessToken: string | null | undefined;
  expiresAt: number | null | undefined;
}

const AuthenticationContext = createContext<AuthenticationState>({
  accessToken: null,
  expiresAt: null,
  checkingSession: false,
  setCheckingSession: () => {},
});

export function AuthenticationContextProvider({
  children,
  accessToken,
  expiresAt,
}: AuthenticationProps) {
  const [checkingSession, setCheckingSession] = useState(false);

  return (
    <AuthenticationContext.Provider
      value={{ accessToken, expiresAt, checkingSession, setCheckingSession }}
    >
      {children}
    </AuthenticationContext.Provider>
  );
}

export default AuthenticationContext;
