import { createContext, PropsWithChildren } from 'react';

interface DemoModeState {
  isDemoMode: boolean;
}

interface DemoModeProps extends PropsWithChildren {
  value: DemoModeState;
}

const DemoModeContext = createContext<DemoModeState>({
  isDemoMode: false,
});

export function DemoContextProvider({ children, value }: DemoModeProps) {
  return (
    <DemoModeContext.Provider value={value}>
      {children}
    </DemoModeContext.Provider>
  );
}

export default DemoModeContext;
