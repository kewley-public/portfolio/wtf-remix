import type { LinksFunction, LoaderFunction } from '@remix-run/node';
import {
  isRouteErrorResponse,
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useRouteError,
  useLoaderData,
  useMatches,
} from '@remix-run/react';
import Error from '~/components/util/Error';

import styles from './tailwind.css';
import { PropsWithChildren } from 'react';
import MainHeader from '~/components/navigation/MainHeader';
import GlobalContextProvider from '~/store';
import SidePanel from '~/components/ui/SidePanel';
import Notification from '~/components/ui/Notification';
import { getAccessTokenFromSession } from '~/api/auth/auth-api.server';

import datePickerStyle from 'react-datepicker/dist/react-datepicker.css';
import AppButton from '~/components/ui/AppButton';

export const links: LinksFunction = () => [
  { rel: 'stylesheet', href: styles },
  { rel: 'stylesheet', href: datePickerStyle },
  {
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css2?family=Rubik:wght@400;700&display=swap',
  },
];

export const loader: LoaderFunction = async ({ request }) =>
  getAccessTokenFromSession(request);

export default function App() {
  const accessToken = useLoaderData<typeof loader>();
  return (
    <Document>
      <GlobalContextProvider accessToken={accessToken}>
        <MainHeader />
        <main className='container mx-auto mt-40'>
          <Outlet />
        </main>
        <div id='notifications'></div>
        <Notification />
        <SidePanel />
      </GlobalContextProvider>
    </Document>
  );
}

interface Props extends PropsWithChildren {
  title?: string;
}

function Document({ children }: Props) {
  const matches = useMatches();

  // @ts-expect-error disableJs is passed from client
  const disableJS = matches.some((match) => match.handle?.disableJS);

  return (
    <html lang='en'>
      <head>
        <meta charSet='utf-8' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <Meta />
        <Links />
      </head>
      <body className='bg-gradiant-ellipse-at-top-left from-primary-300 to-primary-800 min-h-screen m-0 text-gray-100'>
        <main>{children}</main>
        <ScrollRestoration />
        {!disableJS && <Scripts />}
        <LiveReload />
      </body>
    </html>
  );
}

export function ErrorBoundary() {
  const error = useRouteError();

  if (isRouteErrorResponse(error)) {
    return (
      <Document>
        <Error title='Uh oh!'>
          <p>Status: {error.status}</p>
          <p>{error.data.message || 'An unexpected issue occurred'}</p>
          <div className='mt-5'>
            <AppButton to='/'>Back to safety</AppButton>
          </div>
        </Error>
      </Document>
    );
  }
  let errorMessage = 'UnknownError';
  // @ts-expect-error error is a generic type
  if (error?.message) {
    // @ts-expect-error error is a generic type
    errorMessage = error.message;
  }

  return (
    <Document>
      <Error title='An unexpected error occurred'>
        <p>{errorMessage}</p>
        <div className='mt-5'>
          <AppButton to='/'>Back to safety</AppButton>
        </div>
      </Error>
    </Document>
  );
}
