const CONFIG: App.Config = {
  BASE_SERVER_API: process.env.BASE_SERVER_API,
};

export default CONFIG;
