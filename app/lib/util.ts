import { FormikErrors } from 'formik';
import { DateTime } from 'luxon';

const REMAINING_TOKEN_EXPIRY_TIME_ALLOWED = 60 * 1000; // 1 minute before token should be refreshed

export function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function formatCurrency(value: number): string {
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  }).format(value);
}

export function formatMonthYear(year: number, month: number) {
  const date = new Date(+year, +month);
  return date.toLocaleDateString('default', {
    month: 'long',
    year: 'numeric',
  });
}

export function parseErrorMessage(
  error:
    | string[]
    | string
    | null
    | undefined
    | FormikErrors<any>
    | FormikErrors<any>[]
): string | null | undefined {
  if (!error) {
    return null;
  } else if (typeof error === 'string') {
    return error;
  } else if (Array.isArray(error)) {
    // Assuming the array can contain strings or FormikErrors
    return error
      .map((e) => (typeof e === 'string' ? e : Object.values(e).join(', ')))
      .join('; ');
  } else if (typeof error === 'object') {
    // Assuming error is a FormikError object
    return Object.values(error).join(', ');
  }
  return null;
}

/**
 * Checks to see if the access token has reached the threshold
 *
 * @param expiresAt - A access token's expiration
 */
export function isSessionExpiringSoon(expiresAt?: number | null | undefined) {
  if (expiresAt != null) {
    const currentTimeMS = DateTime.now().toMillis();
    console.info(`${expiresAt - currentTimeMS}ms remaining until expiry`);

    return expiresAt - currentTimeMS <= REMAINING_TOKEN_EXPIRY_TIME_ALLOWED;
  }
  return false;
}

export function roundToNearestHundred(num: number) {
  return Math.ceil(num / 100) * 100;
}

export function groupExpensesByMonth(expenses: App.Expense[]) {
  const expensesByMonth: Record<string, App.Expense[]> = {};

  expenses.forEach((expense) => {
    const expenseDate = DateTime.fromJSDate(new Date(expense.date));
    const month = expenseDate.month - 1; // getMonth() returns a zero-based index (0-11)
    const year = expenseDate.year; // Include year to differentiate between same months across years
    const key = formatMonthYear(+year, +month);

    if (!expensesByMonth[key]) {
      expensesByMonth[key] = [];
    }

    expensesByMonth[key].push(expense);
  });

  return expensesByMonth;
}

export function calculateTotalPerMonth(
  groupedExpenses: Record<string, App.Expense[]>,
  categoryId: string | null = null
): Record<string, number> {
  const monthlyTotals: Record<string, number> = {};

  for (const [monthYear, expenses] of Object.entries(groupedExpenses)) {
    if (categoryId) {
      monthlyTotals[monthYear] = expenses
        .filter((e) => e.categoryId === categoryId)
        .reduce((total, expense) => total + expense.amount, 0);
    } else {
      monthlyTotals[monthYear] = expenses.reduce(
        (total, expense) => total + expense.amount,
        0
      );
    }
  }

  return monthlyTotals;
}
