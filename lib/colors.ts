const colors = {
  primary: {
    50: '#e6f0ff', // Lightest shade
    100: '#b2c8ff',
    200: '#809eff',
    300: '#4e74ff',
    400: '#335eff', // Mid-range shade, good for buttons or accents
    500: '#1a48ff', // Base color
    600: '#1539cc', // Slightly darker, good for active or hover states
    700: '#102a99', // Dark shade, good for text or deep accents
    800: '#0b1c66', // Darker shade, good for headers or important text
    900: '#061033', // Darkest shade, good for footer or subtle backgrounds
  },
  category: {
    one: '#4d70f3ff',
    two: '#cc7a40',
    three: '#5cb037',
    four: '#cc4040',
    five: '#ccc540',
    six: '#ae40cc',
  },
};

export default colors;
