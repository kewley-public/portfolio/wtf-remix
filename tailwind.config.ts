/* eslint-disable */

import type { Config } from 'tailwindcss';
import colors from './lib/colors';

export default {
  content: ['./app/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      // Define custom keyframes
      keyframes: {
        slideInFromLeft: {
          '0%': { transform: 'translateX(-100%)' },
          '100%': { transform: 'translateX(0)' },
        },
        slideInFromRight: {
          '0%': { transform: 'translateX(100%)' },
          '100%': { transform: 'translateX(0)' },
        },
        fadeSlideInFromLeft: {
          '0%': { opacity: '0', transform: 'translateX(-1rem)' },
          '100%': { opacity: '1', transform: 'translateX(0)' },
        },
        fadeSlideInFromRight: {
          '0%': { opacity: '0', transform: 'translateX(1rem)' },
          '100%': { opacity: '1', transform: 'translateX(0)' },
        },
        fadeSlideInFromBottom: {
          '0%': { opacity: '0', transform: 'translateY(1rem)' },
          '100%': { opacity: '1', transform: 'translateY(0)' },
        },
        fadeSlideInFromTop: {
          '0%': { opacity: '0', transform: 'translateY(-1rem)' },
          '100%': { opacity: '1', transform: 'translateY(0)' },
        },
        fadeSlideOutFromTop: {
          '0%': { opacity: '1', transform: 'translateY(0)' },
          '100%': { opacity: '0', transform: 'translateY(1rem)' },
        },
      },
      // Define the animation
      animation: {
        enterLeft: 'slideInFromLeft 0.5s ease-out forwards',
        enterRight: 'slideInFromRight 0.5s ease-out forwards',
        fadeInLeft: 'fadeSlideInFromLeft 1s ease-out forwards',
        fadeInRight: 'fadeSlideInFromRight 1s ease-out forwards',
        fadeInBottom: 'fadeSlideInFromBottom 1s ease-out forwards',
        fadeInTop: 'fadeSlideInFromTop 1s ease-out forwards',
        fadeOutTop: 'fadeSlideOutFromTop 1s ease-out forwards',
      },
      backgroundImage: (theme) => ({
        'gradiant-ellipse-at-top-left':
          'radial-gradient(ellipse at top left, var(--tw-gradient-stops))',
      }),
      colors: {
        ...colors,
      },
    },
  },
  plugins: [],
} satisfies Config;
