# Project Name: Where's The Funds

This project is a simple application designed to track and manage your funds and expenses efficiently.
It's built using Remix, TailwindCSS for styling, and Serverless Stack (SST) to manage our serverless
infrastructure on AWS. The application allows users to input, view, update, and delete expenses, providing a clear
and interactive interface. Below are the steps and guidelines for setting up and running the development environment.

## Prerequisites

- Node.js installed (Preferably the latest LTS version)
- An AWS account and AWS CLI configured on your machine
- Familiarity with Next.js, TailwindCSS, and AWS services
- [dotenv-cli](https://github.com/entropitor/dotenv-cli#readme) utilized for `npm run start:local` command to load .env variables

## Setting Up Your Development Environment

### 1. Clone the Repository

Clone the project repository to your local machine using:

```bash
git clone git@gitlab.com:kewley-public/portfolio/wtf-remix.git
```

### 2. Install Dependencies

Navigate to the project directory and install the necessary dependencies:

```bash
cd wtf-remix
npm install
```

### 3. Configure AWS Credentials

Ensure that your AWS credentials are configured by setting up the AWS CLI or configuring environment variables.
You'll need appropriate permissions to deploy resources to AWS.

### 4. Initialize Your SST Development Environment

Start your SST development environment. This command provisions a development environment in your AWS account.
It deploys a debug stack that the SST uses and starts the Live Lambda Development environment.

```bash
npx sst dev
```

Keep this process running during development.

### 5. Start Remix Development Server

In a new terminal window, run your Remix development server:

```bash
npm run dev
```

This command starts the Remix development server, typically available at `http://localhost:3000` unless otherwise configured.

## Run the compiled Remix app

To run the compiled remix application first install the [dotenv-cli](https://github.com/entropitor/dotenv-cli#readme)

Next run:

```bash
npm run build
```

This will create a `build` directory at your project root.

Next run:

```bash
npm run start:local
```

This command will start up a local web server to host your application. It essentially simulates what the code will do
in the cloud.

## Development Workflow

- **Developing Lambda Functions:** With `sst dev` running, your Lambda functions are live-reloaded as you develop, allowing for a seamless integration between your front-end and back-end.
- **Front-end Development:** Utilize TailwindCSS for styling within your Next.js application. Live reloading is enabled by default when running `npm run dev`.

## Deploying to Production

When you're ready to deploy to production, use the following SST command:

```bash
npx sst deploy --stage prod
```

This command deploys your application stack to a "prod" stage, separate from your development environment.

### 6. Cleanup

Once you are done testing locally please clean up your dev resources. You don't have to run this everytime but
it's good practice to do so if you are done for the day.

```bash
npx sst remove
```

Or

```bash
npm run remove 
```

## GitLab Pipeline Process

### Overview

Our project uses a GitLab CI/CD pipeline to automate the processes of linting, testing, building, and deploying our
application. The pipeline consists of four main stages:

1. **Lint** - Checks the codebase for any syntactical errors and coding style issues.
2. **Test** - Runs unit and integration tests to ensure code reliability and functionality.
3. **Build** - Compiles the code into a build artifact that can be deployed.
4. **Deploy** - Manually triggered, this stage deploys the build artifact to the production environment.

### Runner Configuration

The GitLab Runner is configured on a personal Mac Mini. This setup is chosen for cost-effectiveness and the ability
to have more control over the build environment. It's important to note:

- **Cost Savings**: By utilizing our own hardware, we avoid the costs associated with cloud-based runners.
- **Performance**: The Mac Mini is dedicated to running these jobs, ensuring that our pipeline runs are fast and reliable.

### Pipeline Caching

To optimize the pipeline execution time, we use caching strategies, especially for node dependencies.
This means `npm install` won't be run unnecessarily if there are no changes to the package.json or package-lock.json files.

### Security Audits

An audit job is included in the pipeline to scan for vulnerabilities in our dependencies.
This ensures that we maintain a secure codebase.

### Running the Pipeline

To initiate the CI/CD process, simply push your code to the repository. The pipeline will run automatically.
For the deployment stage, you will need to manually trigger the job, allowing for final
reviews or coordination before changes go live.

**Note**: Ensure that the GitLab Runner installed on the Mac Mini is properly registered and configured with your
GitLab instance to execute the defined jobs.

By leveraging GitLab CI/CD with our Mac Mini runner, we maintain a cost-effective, efficient, and secure development
workflow for our expenses management application.

## Additional Notes

- **Environment Variables:** Place any required environment variables in a `.env.local` file for local development and configure them in your AWS environment for production.
- **SST Bind Command:** The `sst bind next dev` command is used to synchronize your SST and Next.js development environments. It's typically used after `sst dev` has started to connect your SST outputs (like API endpoints) to your Next.js environment.
- **Updating Your Stack:** Modify your infrastructure as code in the `stacks/` directory of your SST application.

## Getting Help

- For issues with Remix, consult the [Remix Documentation](https://remix.run/docs/en/main).
- For TailwindCSS, refer to the [TailwindCSS Documentation](https://tailwindcss.com/docs).
- For SST specifics, visit the [Serverless Stack Documentation](https://docs.serverless-stack.com/).
