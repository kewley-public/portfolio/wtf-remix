import { z } from 'zod';
import {
  CategorySchema,
  CreateExpenseSchema,
  EventSchema,
  ExpenseSchema,
  FundSchema,
  UserAuthFormSchema,
  UserSchema,
} from '~/models';

declare global {
  namespace App {
    type Expense = z.infer<typeof ExpenseSchema>;
    type CreateExpense = z.infer<typeof CreateExpenseSchema>;
    type Fund = z.infer<typeof FundSchema>;
    type Category = z.infer<typeof CategorySchema>;
    type Event = z.infer<typeof EventSchema>;
    type User = z.infer<typeof UserSchema>;
    type UserAuthForm = z.infer<typeof UserAuthFormSchema>;
    type AuthorizedUserResponse = {
      id: string;
      name: string | null | undefined;
      accessToken: string;
      refreshToken: string;
      expiresAt: number;
    };
    type RegistrationResponse = {
      email: string;
      passwordHash?: string;
      userName?: string;
      googleOAuthId?: string;
    };
    type Paginated<T> = {
      items: T[];
      lastEvaluatedKey: Record<string, string>;
    };

    interface Config {
      BASE_SERVER_API: string | undefined;
    }

    interface Notification {
      status: string;
      title: string;
      message: string;
    }

    interface AccessToken {
      token: string;
      expiresAt: number;
    }
  }
}

export default {};
