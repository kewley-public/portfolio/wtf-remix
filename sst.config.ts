/* eslint-disable */

import { SSTConfig } from 'sst';
import { RemixSite } from 'sst/constructs';
import * as secretsmanger from 'aws-cdk-lib/aws-secretsmanager';
import dotenv from 'dotenv';

dotenv.config();

export default {
  config(_input) {
    return {
      name: 'wtf-remix',
      region: 'us-east-1',
    };
  },
  stacks(app) {
    app.stack(function Site({ stack }) {
      const wtfEnvironmentConfig = secretsmanger.Secret.fromSecretNameV2(
        stack,
        'WtfEnvironmentConfig',
        'wtfEnvironmentConfig'
      );

      const site = new RemixSite(stack, 'site', {
        timeout: 35,
        environment: {
          BASE_SERVER_API:
            process.env.BASE_SERVER_API ||
            'https://3yaqxp8pr2.execute-api.us-east-1.amazonaws.com/prod',
          GOOGLE_CLIENT_ID: wtfEnvironmentConfig
            .secretValueFromJson('GOOGLE_CLIENT_ID')
            .unsafeUnwrap()
            .toString(),
          GOOGLE_CLIENT_SECRET: wtfEnvironmentConfig
            .secretValueFromJson('GOOGLE_CLIENT_SECRET')
            .unsafeUnwrap()
            .toString(),
          SESSION_SECRET: wtfEnvironmentConfig
            .secretValueFromJson('JWT_SECRET')
            .unsafeUnwrap()
            .toString(),
          NODE_ENV: 'production',
          GOOGLE_CALLBACK:
            'https://d1dlez7vx6xypq.cloudfront.net/auth/google/callback',
        },
      });

      stack.addOutputs({
        SiteUrl: site.url,
      });
    });
  },
} satisfies SSTConfig;
